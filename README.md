# ThesaurusManager

Installation
------------

1. Configure settings file
	```console
	$ cp ./ThesaurusManager/local_settings.py.example ./ThesaurusManager/local_settings.py
	$ $EDITOR ./ThesaurusManager/local_settings.py
	```
2.	If a PostgreSQL database is used, make sure to create it, first:
	```console
	$ createdb ...
	```
3.	Run the initialiser
	```console
	$ make init
	'/opt/local/bin/python3.6' -m venv './venv'
	./venv/bin/python -m pip install  --disable-pip-version-check -r './requirements.txt'
	Collecting Django==2.1.5 (from -r ./requirements.txt (line 1))
	...
	./venv/bin/python manage.py migrate
	Operations to perform:
	  Apply all migrations: TM, admin, auth, contenttypes, sessions
	Running migrations:
	...
	Creating super user account...
	./venv/bin/python manage.py createsuperuser
	Username (leave blank to use 'root'): root
	Email address: root@localhost
	Password:
	Password (again):
	Superuser created successfully.
	Now, run ThesaurusManager using `make serve'.
	```
4.	Run  
	**a. development server**  
	```console
	$ make serve
	```

	**b. production**  
	Copy static files to separate directory.

	```console
	$ venv/bin/python3 ./manage.py collectstatic --no-input
	```

	Then configure the web application server to serve static files directly
	and use WSGI (`ThesaurusManager/wsgi.py`).
