# Variables
SRC_DIR := ./TM
# DESIRED_PYTHON_VERSION: only major.minor allowed
DESIRED_PYTHON_VERSION := 3.6
# MIN_PYTHON_VERSION: major.minor.micro allowed
MIN_PYTHON_VERSION := 3.5
VENV := ./venv

# OS detection
ifeq ($(OS),Windows_NT)
$(error Windows NT is not supported. Things are going to break)
endif

# Usage: $(call is_min_python_version,/path/to/python,min_version_str)
# prints 1 if greater or equal, nothing
define is_min_python_version
$(shell echo $(2) | $(1) -c 'import sys; min = tuple(map(int, sys.stdin.readline().split("."))); sys.stdout.write("1" * (sys.version_info[:len(min)] >= min))')
endef

# Usage: $(call python_version,/path/to/python)
define python_version
$(shell $(1) -c 'import sys; sys.stdout.write(".".join([str(x) for x in sys.version_info[:3]]))')
endef

# Path detection
BASE_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
ifneq ($(BASE_DIR),$(shell pwd -P || echo "$$PWD")/)
# Ensure that all commands are executed in the BASE_DIR.
%: ; $(MAKE) -C '$(BASE_DIR)' $@
else

# Find the Python binary on the system that matches most closely matches $(DESIRED_PYTHON_VERSION)
DESIRED_PYTHON_VERSION_PARTS := $(subst ., ,$(DESIRED_PYTHON_VERSION))
SYSTEMPYTHON := '$(firstword $(PYTHON) $(shell command -v \
	python$(word 1,$(DESIRED_PYTHON_VERSION)).$(word 2,$(DESIRED_PYTHON_VERSION)) \
	python$(word 1,$(DESIRED_PYTHON_VERSION)) \
	python ;))'

VENVPYTHON := $(VENV)/bin/python

# Unset PYTHONHOME environment variable, because that's what a virtualenv's
# activate script does, too
unexport PYTHONHOME

# Helper command to install using Pip into the virtualenv
PIP_INSTALL = \
$(if $(CFLAGS),CFLAGS='$(CFLAGS)') \
$(if $(LDFLAGS),LDFLAGS='$(LDFLAGS)') \
$(if $(ARCHFLAGS),ARCHFLAGS='$(ARCHFLAGS)') \
$(VENVPYTHON) -m pip install $(PIPFLAGS) --disable-pip-version-check


# Help

.PHONY: help
help:
	$(info )
	$(info Welcome to ThesaurusManager.)
	$(info )
	$(info You can choose between these targets:)
	$(info )
	$(info * help				Display this help page.)
	$(info )
	$(info ThesaurusManager:)
	$(info * virtualenv			Create a virtual environment.)
	$(info * init				Initialise a local installation.)
	$(info * migrate			Runs database migrations on the databases configured in \
								settings.py (initialises them if needed))
	$(info * superuser			Create a Django superuser.)
	$(info * serve				Run a standalone server.)
	$(info * clean				Clean up.)
	$(info Development:)
	$(info * lint				Run the linter.)
	$(info )
	@: # noop (needed to suppress "nothing to be done")


# Virtualenv targets

$(VENV)/bin/activate:
ifeq ($(SYSTEMPYTHON),'')
	$(error Cannot find Python. Please make sure that Python is in PATH)
endif
# Check Python version
ifeq ($(call is_min_python_version,$(SYSTEMPYTHON),$(MIN_PYTHON_VERSION)),)
	$(error Python version $(MIN_PYTHON_VERSION) needed. Only $(call python_version,$(SYSTEMPYTHON)) found)
endif
# Configure virtualenv
	$(SYSTEMPYTHON) -m venv '$(VENV)'

.PRECIOUS: $(VENV)/.requirements_installed
$(VENV)/.requirements_installed: requirements.txt ThesaurusManager/settings.py $(wildcard ThesaurusManager/local_settings.py) | $(VENV)/bin/activate
# Install TM requirements
	$(PIP_INSTALL) -r './requirements.txt'
	$(VENVPYTHON) -c 'from ThesaurusManager.settings import *; print(*(d["ENGINE"] for d in DATABASES.values()), sep="\n")' | sort -u | while read -r engine; do \
		case $$engine in \
		(django.db.backends.postgresql) \
			$(PIP_INSTALL) psycopg2;; \
		esac; \
	done
	@touch '$@'

$(VENV)/.flake8_installed: | $(VENV)/bin/activate
	$(PIP_INSTALL) flake8
	@touch '$@'

$(VENV)/.superuser_created: $(VENV)/.db_migrated | $(VENV)/bin/activate
	$(MAKE) migrate
# Create super user account
	@echo Creating super user account...
	$(VENVPYTHON) manage.py createsuperuser
	@touch '$@'

# PHONY targets
.PHONY: virtualenv
virtualenv: $(VENV)/.requirements_installed $(VENV)/bin/activate
	@echo Created virtualenv in "$(realpath $(VENV))".


.PHONY: superuser
superuser: $(VENV)/.superuser_created


.PHONY: init
init: | $(VENV)/.superuser_created
	@echo Now, run ThesaurusManager using \`make serve\'.


.PHONY: migrate
migrate: | $(VENV)/.requirements_installed
	$(VENVPYTHON) manage.py migrate


.PHONY: serve
serve: | $(VENV)/.requirements_installed
	$(VENVPYTHON) manage.py runserver --insecure


.PHONY: clean
clean:
	$(RM) -R $(VENV)
	$(RM) ./*coverage*.xml
	find $(SRC_DIR) -iname '*.pyc' -delete
	find $(SRC_DIR) -type d -name '__pycache__' -delete


.PHONY: lint
lint: | $(VENV)/.flake8_installed
	$(VENVPYTHON) -m flake8 --show-source $(SRC_DIR)

endif
