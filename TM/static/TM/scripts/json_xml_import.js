// JSON import and validation
function selectJSONFileAndValidate(processJSONFunction) {
	selectFileAndValidate(
		"JSON",
		"application/json",
		function(file) {
			return JSON.parse(file);
		},
		processJSONFunction
	);
}

// XML import and validation
function selectXMLFileAndValidate(processJSONFunction) {
	selectFileAndValidate("XML", "text/xml", function (file) {
			var xml = $.parseXML(file);
			var x2js = new X2JS({
				arrayAccessFormPaths: [
					/.*\.children$/,
					/.*\.labels$/,
					/.*\.variants$/,
					/.*\.definitions$/,
					/.*\.references$/,
					/.*\.external_references$/,
					/.*\.sort_order_hits$/,
					],
				emptyNodeForm: "object"
			});
			var json = x2js.xml2json(xml);
			cleanJSONafterTransformation(json["thesaurus"]);
			return json["thesaurus"];
		}, processJSONFunction);
}

function cleanJSONafterTransformation(json) {
	var key;
	for (key in json) {
		if ($.isArray(json[key])) {
			if (1 === json[key].length && testIfEmptyObj(json[key][0])) {
				json[key] = null;
			} else {
				for (var i = 0; i < json[key].length; i++) {
					cleanJSONafterTransformation(json[key][i]);
				}
			}
		} else if ("id" === key || key.endsWith("_id") || "position_hit" === key) {
			// Integer attributes
			var integer = parseInt(json[key], 10);
			if (!isNaN(integer)) {
				json[key] = integer;
			}
		} else if (testIfEmptyObj(json[key])) {
			json[key] = ("identifier" === key ? null : "");
		}
	}

	function testIfEmptyObj(obj) {
		return ("object" === typeof(obj) && $.isEmptyObject(obj));
	}
}

// Generic file import and validation
function selectFileAndValidate(title, fileType, parseAndProcessFile, processJSONFunction) {
	$("<input type='file' id='file' accept='" + fileType + "' />")
		.on("change", function (event) {
			var myReader = new FileReader();
			myReader.addEventListener("loadend", function (e) {
				try {
					var json = parseAndProcessFile(e.srcElement.result);
					var validate = validateJSON(json);
				} catch (e) {
					var validate = {
							success: false,
							errors: e
					};
				}
				if (validate.success) {
					processJSONFunction(json);
				} else {
					console.log(validate.errors);
					var errorDescription = "<h3>Fehlerhafte " + title + "-Datei</h3>";
					if (validate.errors instanceof SyntaxError) {
						errorDescription += "<div class='failure_box'>";
						errorDescription += "<h5>Fehlermeldung:</h5><p class='error_message'>" + validate.errors.message + "</p>";
						errorDescription += "</div>";
					} else {
						for (var i = 0; i < validate.errors.length; i++) {
							errorDescription += "<div class='failure_box'>";
							errorDescription += "<h4>Fehler #" + (i + 1) + "</h4>";
							errorDescription += "<h5>Fehlermeldung:</h5><p class='error_message'>" + validate.errors[i].message + "</p>";
							errorDescription += "<h5>Pfad:</h5><p class='error_message'>" + validate.errors[i].dataPath + "</p>";
							errorDescription += "</div>";
						}
					}
					messageBox(errorDescription, title + "-Import");
				}
			});
			myReader.readAsText(this.files[0]);
		})
		.click();
}

// JSON validation
function validateJSON(json) {
	var ajv = new Ajv({"allErrors": true});
	var validate = ajv.compile(json_schema);
	return {
		"success": validate(json),
		"errors": validate.errors
	}
}
