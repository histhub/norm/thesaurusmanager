/**
 * For further information see:
 * http://bl.ocks.org/Rokotyan/0556f8facbaf344507cdc45dc3622177
 * http://bl.ocks.org/wboykinm/e6e222d71e9b59e8b3053e0c4fe83daf
 */

function createExportSection($parent, authenticated) {
	var $exportSection = $('<div id="export_section"/>');

	var $menuContent = createExpandableMenu($exportSection, "Exportieren");
	var sections, i, n, $sectionDiv, formats;

	sections = [{"label": "Bildformate", "formats": ["svg", "png"]}];
	if (authenticated) {
		sections.push({"label": "Textformate", "formats": ["json", "xml"]});
	}
	for (n = 0; n < sections.length; n++) {
		$sectionDiv = $("<div class='subsection'/>")
			.html("<label>" + sections[n].label + "</label>")
			.appendTo($menuContent);

		formats = sections[n].formats;
		for (i = 0; i < formats.length; i++) {
			$("<button class='export_button'>" + formats[i].toUpperCase() + "</button>")
				.on("click", {"format": formats[i]}, startExport)
				.appendTo($sectionDiv);
		}
	}

	$parent.append($exportSection);

	return $menuContent.parent().children(":first");  // expand button

	// Start export
	function startExport(event) {
		var $selectedVocab = $("select#vocab_selection").find("option:selected");
		if (0 === $selectedVocab.length) {
			return;
		}
		var fileName = ($selectedVocab.data("vocab").name + "_" + $selectedVocab.data("vocab").version).replace(/[^0-9a-zA-Z_]+/g, "_");
		var format = event.data.format;

		switch (format) {
		case "json":
			var blob = new Blob([JSON.stringify(traverseTree(rootForSaving), null, 4)], {"type": "application/json"});
			saveAs(blob, fileName + ".json");
			break;
		case "xml":
			var thesaurusToExport = { thesaurus: traverseTree(rootForSaving) };
			var blob = new Blob([(new X2JS()).json2xml_str(thesaurusToExport)], {"type": "text/xml"});
			saveAs(blob, fileName + ".xml");
			break;
		default:
			var svg = d3.select("#tree-container").select("svg");
			var width = svg.attr("width");
			var height = svg.attr("height");
			var svgString = getSVGString(prepareSVG(svg.node()));
			svgString2Image(svgString, 2*width, 2*height, format, save);  // passes Blob and filesize String to the callback

			function save(dataBlob, format) {
				saveAs(dataBlob, fileName + "." + format);  // FileSaver.js function
			}
		}
	}

	//	Below are the functions that handle actual exporting:
	//	getSVGString ( svgNode ) and svgString2Image( svgString, width, height, format, callback )
	function getSVGString(svgNode) {
		svgNode.setAttribute("xlink", "http://www.w3.org/1999/xlink");
		var cssStyleText = getCSSStyles(svgNode);
		appendCSS(cssStyleText, svgNode);

		var serializer = new XMLSerializer();
		var svgString = serializer.serializeToString(svgNode);
		svgString = svgString.replace(/(\w+)?:?xlink=/g, "xmlns:xlink=");  // Fix root xlink without namespace
		svgString = svgString.replace(/NS\d+:href/g, "xlink:href");  // Safari NS namespace fix

		return svgString;

		function getCSSStyles(parentElement) {
			var selectorTextArr = [];

			// Add Parent element Id and Classes to the list
			selectorTextArr.push("#"+parentElement.id);
			for (var c = 0; c < parentElement.classList.length; c++) {
				if (!contains("."+parentElement.classList[c], selectorTextArr)) {
					selectorTextArr.push('.'+parentElement.classList[c]);
				}
			}

			// Add Children element Ids and Classes to the list
			var nodes = parentElement.getElementsByTagName("*");
			for (var i = 0; i < nodes.length; i++) {
				var id = nodes[i].id;
				if (!contains("#"+id, selectorTextArr)) {
					selectorTextArr.push("#"+id);
				}

				var classes = nodes[i].classList;
				for (var c = 0; c < classes.length; c++) {
					if (!contains("."+classes[c], selectorTextArr)) {
						selectorTextArr.push("."+classes[c]);
					}
				}
			}

			// Extract CSS Rules
			var extractedCSSText = "";
			for (var i = 0; i < document.styleSheets.length; i++) {
				var s = document.styleSheets[i];

				try {
					if (!s.cssRules) { continue; }
				} catch(e) {
					if ("SecurityError" !== e.name) { throw e; /* for Firefox */ }
					continue;
				}

				var cssRules = s.cssRules;
				for (var r = 0; r < cssRules.length; r++) {
					if (contains(cssRules[r].selectorText, selectorTextArr)) {
						extractedCSSText += cssRules[r].cssText;
					}
				}
			}

			return extractedCSSText;

			function contains(str,arr) {
				return (arr.indexOf(str) !== -1);
			}
		}

		function appendCSS(cssText, element) {
			var styleElement = document.createElement("style");
			styleElement.setAttribute("type", "text/css");
			styleElement.innerHTML = cssText;
			var refNode = (element.hasChildNodes() ? element.children[0] : null);
			element.insertBefore(styleElement, refNode);
		}
	}

	function svgString2Image(svgString, width, height, format, callback) {
		switch (format) {
		case "svg":
			var blob = new Blob([svgString], {type: "image/svg+xml"});
			if (callback) {
				callback(blob, format);
			}
			break;
		case "png":
			// Convert SVG string to data URL
			var imgsrc = "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(svgString)));

			var canvas = document.createElement("canvas");
			var context = canvas.getContext("2d");
			canvas.width = width;
			canvas.height = height;

			var image = new Image();
			image.onload = function () {
				context.clearRect (0, 0, width, height);
				context.drawImage(image, 0, 0, width, height);

				canvas.toBlob(function (blob) {
					if ("function" === typeof(callback)) {
						callback(blob, format);
					}
				});
			};

			image.src = imgsrc;
			break;
		}
	}

	function prepareSVG(svg) {
		var essentialElementClasses = ["node", "link", "nodeText", "nodeCircle"];

		function traverseDom(node) {
			var i;
			if (node.children.length) {
				for (i = 0; i < node.children.length; i++) {
					traverseDom(node.children[i]);
				}
			} else {
				if (!essentialElementClasses.includes(node.className.baseVal)) {
					node.parentNode.removeChild(node);
				}
				else {
					node.removeAttribute("style");
					if ("nodeCircle" === node.className.baseVal) {
						node.setAttribute("stroke", "blue");
						node.setAttribute("stroke-width","2");
						node.setAttribute("fill", "white");
					}
				}
			}
		}

		var clone = svg.cloneNode(true);

		var i, neededElements = ["g", "style"];
		for (i = clone.children.length - 1; i >= 0; i--) {
			// Remove not needed elements like the insert and remove buttons
			var child = clone.children[i];
			if (!neededElements.includes(child.tagName)) {
				child.parentNode.removeChild(child);
			}
		}

		traverseDom(clone);
		return clone;
	}
}
