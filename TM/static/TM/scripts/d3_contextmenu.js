
function contextMenu(styleExtension=null, doBefore=null, doBeforeHandler=null, doAfterHandler=null) {
	var margin = 0.075, // fraction of width
	items = [], 
	rescale = false, 
	style = $.extend(
			true,
			{
				'rect': {
					'mouseout': {
						'fill': '#B0C4DE', 
						'stroke': 'white', 
						'stroke-width': '1px'
					}, 
					'mouseover': {
						'fill': '#4169E1'
					}
				},
				'text': {
					'fill': '#00008B',
					'font-size': '12'
				}
			},
			styleExtension),
	offset = 40;
	
	
	// The "starPattern" parameter indicates whether the entries are arranged on the left
	// and right side and at the bottom or whether the menu consists of one simple stack.
	function menu(element, starPattern=false) {
		$("body").trigger("click"); // remove still opened context menus
		
		if (doBefore) {
			doBefore();
		}
		
		if (rescale) {
			var scale = scaleItems(items);
			rescale = false;
		}

		// Draw the menu
		var cMenu = element.append('g').attr('class', 'context-menu');
		var positionCalculator = getPositionCalculator(starPattern);
		
		function baseMouseoverHandler(element) {
			d3.select(element).select('rect').style(style.rect.mouseover);
			d3.selectAll('.sub-menu-entry').style('visibility', 'hidden');
			d3.selectAll('.arrow').style('visibility', 'hidden');
		}
		
		cMenu
		.selectAll('tmp')
		.data(items).enter()
		.append('g').attr('class', 'menu-entry')
		.style({'cursor': 'pointer'})
		.on('mouseover', function() {
			baseMouseoverHandler(this);
		})
		.on('mouseout', function() {
			d3.select(this).select('rect').style(style.rect.mouseout);
		})
		.each(function(d, i) {
			d.width = scale.width;
			d.height = scale.height;
			d.margin = scale.margin;
			d.x = positionCalculator.getX(d, i);
			d.y = positionCalculator.getY(d, i);			
			d.dx = scale.margin;
			d.dy = scale.height - scale.margin / 2;
		});
		
		constructRect(d3.selectAll('.menu-entry'));
		constructText(d3.selectAll('.menu-entry'));

		// submenus
		d3.selectAll('.menu-entry')
		.filter(function(d) { return d.submenu; })
		.each(function(d, i) {
			var subScale = scaleItems(d.submenu);
			var submenu = cMenu.selectAll('tmp')
			.data(d.submenu).enter()
			.append('g')
			.each(function(sub_d, i) {
				sub_d.x = d.x + d.width;
				sub_d.y = d.y + (i * d.height);
				sub_d.width = subScale.width;
				sub_d.height = d.height;
				sub_d.dx = subScale.margin;
				sub_d.dy = d.height - d.margin / 2;
			})
			.attr('class', 'menu-entry')
			.attr('class', 'sub-menu-entry')
			.style({'cursor': 'pointer'})
			.style('visibility', 'hidden')
			.on('mouseover', function() {
				d3.select(this).select('rect').style(style.rect.mouseover);
			})
			.on('mouseout', function() {
				d3.select(this).select('rect').style(style.rect.mouseout);
			});
			
			d3.select(this)
			.on('mouseover', function() {
				baseMouseoverHandler(this);
				d3.select(this).select('.arrow').style('visibility', 'visible');
				submenu.style('visibility', 'visible');
			})
			.append("text")
			.text("\u27A4")
			.attr("class", "arrow")
			.style("text-anchor", "end")
			.attr('x', d.x + d.width)
			.attr('y', d.y)
			.attr('dx', -(d.margin / 4))
			.attr('dy', d.height - d.margin / 2)
			.style(style.text)
			.style('visibility', 'hidden');
		});
		
		constructRect(d3.selectAll('.sub-menu-entry'));
		constructText(d3.selectAll('.sub-menu-entry'));
		
		function constructRect(selection) {
			selection
			.append('rect')
			.attr('x', function(d, i) { return d.x; })
			.attr('y', function(d, i) { return d.y; })
			.attr('width', function(d, i) { return d.width; })
			.attr('height', function(d, i) { return d.height; })
			.style(style.rect.mouseout)
			.on("click", click);
		}
		
		function constructText(selection) {
			selection
			.append('text')
			.text(function(d) { return d.text; })
			.attr('x', function(d, i) { return d.x; })
			.attr('y', function(d, i) { return d.y; })
			.attr('dx', function(d, i) { return d.dx })
			.attr('dy', function(d, i) { return d.dy; })
			.style(style.text)
			.on("click", click);			
		}
		
		// bring context menu to front
		element.each(function() { 
			return d3.select(this).each(function() {
				this.parentNode.appendChild(this);
			});
		});

		// Other interactions
		d3.select('body')
		.on('click', function() {
			constructCompleteHandler(null);
		});
		
		// click event handler
		function click(d) {
			d3.event.stopPropagation();
			if (d.handler) {
				constructCompleteHandler(d.handler);
			}
		}

		// generic handler
		function constructCompleteHandler(handler) {
			d3.select('body').on('click', null);
			cMenu.remove();
			if (doBeforeHandler) {
				doBeforeHandler();
			}
			if (handler) {
				handler();
			}
			if (doAfterHandler) {
				doAfterHandler();
			}
		}
		
		// returns the functions needed for calculating the position of the entries
		function getPositionCalculator(starPattern) {
			if (!starPattern) {
				return {
					getX: function(d, i) {
						return offset;
					},
					getY: function(d, i) {
						return i * d.height;
					}
				}
			}
			else {
				var right = 0, left = 0, bottom = 0;
				return {
					getX: function(d, i) {
						switch(d.position) {
						case "right":
							return Math.floor(d.width / 2);
							break;
						case "left":
							return -Math.floor(3 * d.width / 2);
							break;
						case "bottom":
							return -Math.floor(d.width / 2);
							break;
						default:
							break;
						}
					},
					getY: function(d, i) {
						switch(d.position) {
						case "right":
							return right++ * d.height;
							break;
						case "left":
							return left++ * d.height;
							break;
						case "bottom":
							return offset + (bottom++ * d.height);
							break;
						default:
							break;						
						}
					}
				}
			}
		}
	}

	menu.items = function(e) {
		if (!arguments.length) return items;
		for (var i in arguments) items.push(arguments[i]);
		rescale = true;
		return menu;
	}

	// Automatically set width, height, and margin;
	function scaleItems(items) {
		d3.select('svg').selectAll('tmp')
		.data(items).enter()
		.append('text')
		.text(function(d){ return d.text; })
		.style(style.text)
		.attr('x', -1000)
		.attr('y', -1000)
		.attr('class', 'tmp');
		var z = d3.selectAll('.tmp')[0]
		.map(function(x){ return x.getBBox(); });
		var width = d3.max(z.map(function(x) { return x.width; }));
		var margin_scaled = margin * width;
		width =  width + 3 * margin_scaled;
		var height = d3.max(z.map(function(x) { return x.height + margin_scaled / 2; }));

		// cleanup
		d3.selectAll('.tmp').remove();
		
		return {
			width: width,
			height: height,
			margin: margin_scaled
		};
	}

	return menu;
}
