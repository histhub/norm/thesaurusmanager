// Generic confirm box
function confirmDialog(message, title, yesFunction, noFunction) {
	$("<div/>")
		.html("<div><h6>" + message + "</h6></div>")
		.dialog({
			"modal": true,
			"title": title,
			"autoOpen": true,
			"resizable": false,
			"position": {
				"my": "center",
				"at": "top+25%",
				"of": window
			},
			"buttons": {
				"Ja": function () {
					if ("function" === typeof(yesFunction)) {
						yesFunction();
					}
					$(this).dialog("close");
				},
				"Nein": function () {
					if ("function" === typeof(noFunction)) {
						noFunction();
					}
					$(this).dialog("close");
				}
			},
			"close": function (event, ui) {
				$(this).remove();
			}
		});
}


// Function for creating a confirm box for saving the tree
function getConfirmationBoxForSavingTree(callback) {
	function createCloseCross($modalContent) {
		$("<span class='close'>&times;</span>")
			.on("click", function (event) {
				$modalContent.closest(".modal").remove();
			})
			.prependTo($modalContent);
	}

	confirmDialog(
		"Möchten Sie zunächst allfällige Änderungen speichern?",
		"Speicherung",
		function () {
			var $modal = $("<div id='save_confirmation' class='modal'/>");
			var $modalContent = $("<div class='modal-content'/>").appendTo($modal);
			var $modalMessage = $("<p/>")
				.html("Daten werden gespeichert&hellip;")
				.appendTo($modalContent);

			saveTree(function (json) {
				if (json && json.success) {
					if ("function" === typeof(callback)) {
						callback(true);
					} else {
						$modalMessage.text("Speicherung fehlgeschlagen");
						createCloseCross($modalContent);
					}
				}
			}, function () {
				$modalMessage.text("Speicherung fehlgeschlagen");
				createCloseCross($modalContent);
			}, function () {});
			$modal.appendTo(document.body);
		},
		function () {
			if ("function" === typeof(callback)) { callback(false); }
		}
	);
}

// Generic message box
function messageBox(message, title, buttonFunction) {
	buttonFunction = ("undefined" !== typeof(buttonFunction) ? buttonFunction : null);
	$("<div/>")
		.html("<div><h6>" + message + "</h6></div>")
		.dialog({
			"modal": true,
			"title": title,
			"autoOpen": true,
			"resizable": false,
			"position": {
				"my": "center",
				"at": "top+25%",
				"of": window
			},
			"buttons": {
				"Close": function () {
					if ("function" === typeof(buttonFunction)) {
						buttonFunction();
					}
					$(this).dialog("close");
				}
			},
			"close": function (event, ui) {
				$(this).remove();
			}
		});
}
