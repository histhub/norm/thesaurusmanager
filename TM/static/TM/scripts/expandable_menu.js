function createExpandableMenu($expandSection, openButtonCaption) {
	var $expandableControls = $('<div class="dropdown-content"/>');

	$expandSection
		.addClass("dropdown")
		.append($('<button class="expandable_section_button"/>')
				.text(openButtonCaption))
		.append($expandableControls);

	return $expandableControls;
}
