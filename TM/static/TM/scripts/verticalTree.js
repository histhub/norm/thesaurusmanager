/*Copyright (c) 2013-2016, Rob Schmuecker
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* The name Rob Schmuecker may not be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL BOSTOCK BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

var rootForSaving = null;
var vocab_id = null; // needed for the AJAX call for tree modifications

var searchInputCache = []; // search autocomplete cache



function constructTree(sourceJSON, treeContainerSelector, load_status, elements, search_section, language_selection, reference = null, referencedConceptSelectionAllowed = false) {
	var root = null;

	if (load_status) { // complete mode
		var completeMode = true;
		rootForSaving = null;
		vocab_id = null;

		for (var n = 0; n < elements.hideAndReactivateOnlyIfModifiable.length; n++) {
			elements.hideAndReactivateOnlyIfModifiable[n].hide();
		}

		var switches = [];
		for (var n = 0; n < elements.deactivateWhileTreeConstructing.length; n++) {
			switches.push(controlElementActivation(elements.deactivateWhileTreeConstructing[n]));
		}
		setStatus(load_status, "Daten werden geladen ...", 20);
		search_section.empty();
	} else {
		var completeMode = false;
	}



	// Get JSON data
	treeJSON = d3.json(sourceJSON, function (error, data) {

		// preparations
		// examine delivered data
		if (!data || !data["tree"] || !data["vocab_id"]) {
			if (completeMode) {
				finalization(switches, load_status, "Daten nicht verfügbar", true);
			}
			return;
		}

		var authenticated = false;
		var modification_allowed = false;
		if (completeMode) {
			vocab_id = data["vocab_id"];

			if ("undefiend" !== typeof(data["authenticated"])) {
				authenticated = !!data["authenticated"];
			}

			if ("undefined" !== typeof(data["modification_allowed"])) {
				modification_allowed = !!data["modification_allowed"];
			}

			for (var n = 0; n < elements.hideAndReactivateOnlyIfModifiable.length; n++) {
				elements.hideAndReactivateOnlyIfModifiable[n].toggle(modification_allowed);
			}

			setStatus(load_status, "Daten werden geladen ...", 50);
		}

		treeData = data["tree"];


		// Calculate total nodes, max label length
		var totalNodes = 0;
		var maxLabelLength = 0;
		// variables for drag/drop
		var selectedNode = null;
		var draggingNode = null;
		// panning variables
		var panSpeed = 400;
		var panBoundary = 0; // Within 20px from edges will pan when dragging.
		// Misc. variables
		var i = 0;
		var duration = 750;


		// size of the diagram
		var viewerWidth = $(treeContainerSelector).width();
		var viewerHeight = $(treeContainerSelector).height();

		var tree = d3.layout.tree()
			.size([viewerWidth, viewerHeight]);

		// define a d3 diagonal projection for use by the node paths later on.
		var diagonal = d3.svg.diagonal()
			.projection(function(d) {
				return [d.x, d.y];
			});

		// A recursive helper function for performing some setup by walking through all nodes

		function visit(parent, visitFn, childrenFn) {
			if (!parent) return;

			visitFn(parent);

			var children = childrenFn(parent);
			if (children) {
				var count = children.length;
				for (var i = 0; i < count; i++) {
					visit(children[i], visitFn, childrenFn);
				}
			}
		}

		// Call visit function to establish maxLabelLength
		visit(treeData, function (d) {
			totalNodes++;
			maxLabelLength = Math.max(getNodeLabel(d).length, maxLabelLength);
		}, function (d) {
			return d.children && d.children.length > 0 ? d.children : null;
		});

		// sort the tree according to the sort order hits and labels
		function sortTree() {
			tree.sort(function (a, b) {
				if (a.sort_order_hits && b.sort_order_hits) {
					var sortOrderDiff = a.sort_order_hits[0]["position_hit"] - b.sort_order_hits[0]["position_hit"];
					if (sortOrderDiff != 0) {
						return sortOrderDiff;
					} else {
						return sortAlphabetically(a, b);
					}
				} else if (a.sort_order_hits) {
					return 1;
				} else if (b.sort_order_hits) {
					return -1;
				} else {
					return sortAlphabetically(a, b);
				}
			});
		}
		// alphabetical sort order
		function sortAlphabetically(a, b) {
			var lblA = getNodeLabel(a).toLowerCase();
			var lblB = getNodeLabel(b).toLowerCase();
			return ((lblA === lblB) ? 0 : ((lblA > lblB) ? 1 : -1));
		}

		// Sort the tree initially incase the JSON isn't sorted
		sortTree();

		// TODO: Pan function, can be better implemented.

		function pan(domNode, direction) {
			var speed = panSpeed;
			if (panTimer) {
				clearTimeout(panTimer);
				translateCoords = d3.transform(svgGroup.attr("transform"));
				if (direction === "left" || direction === "right") {
					translateX = (direction == 'left') ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
					translateY = translateCoords.translate[1];
				} else if (direction === 'up' || direction === 'down') {
					translateX = translateCoords.translate[0];
					translateY = (direction == 'up') ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
				}
				scaleX = translateCoords.scale[0];
				scaleY = translateCoords.scale[1];
				scale = zoomListener.scale();
				svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
				d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX + "," + translateY + ")");
				zoomListener.scale(zoomListener.scale());
				zoomListener.translate([translateX, translateY]);
				panTimer = setTimeout(function () {
					pan(domNode, speed, direction);
				}, 50);
			}
		}

		// Define the zoom function for the zoomable tree

		function zoom() {
			svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
		}


		// define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
		var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

		function initiateDrag(d, domNode) {
			draggingNode = d;
			d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
			d3.select(treeContainerSelector).selectAll('.ghostCircle').attr('class', 'ghostCircle show');
			d3.select(domNode).attr('class', 'node activeDrag');

			svgGroup.selectAll("g.node").sort(function (a, b) { // select the parent and sort the path's
				if (a.id != draggingNode.id) {
					// a is not the hovered element, send "a" to the back
					return 1;
				} else {
					// a is the hovered element, bring "a" to the front
					return -1;
				}
			});
			// if nodes has children, remove the links and nodes
			if (nodes.length > 1) {
				// remove link paths
				links = tree.links(nodes);
				nodePaths = svgGroup.selectAll("path.link")
					.data(links, function (d) {
						return d.target.id;
					}).remove();
				// remove child nodes
				nodesExit = svgGroup.selectAll("g.node")
					.data(nodes, function (d) {
						return d.id;
					}).filter(function (d, i) {
						return (d.id != draggingNode.id);
					}).remove();
			}

			// remove parent link
			parentLink = tree.links(tree.nodes(draggingNode.parent));
			svgGroup.selectAll('path.link').filter(function (d, i) {
				return (d.target.id == draggingNode.id);
			}).remove();

			dragStarted = null;
		}

		// define the baseSvg, attaching a class for styling and the zoomListener
		var baseSvg = d3.select(treeContainerSelector).append("svg")
			.attr("width", viewerWidth)
			.attr("height", viewerHeight)
			.attr("class", "overlay")
			.call(zoomListener);

		$(window).on("resize", function () {
			var $container = $("#tree-container");
			var $svg = $container.find("svg.overlay");

			$svg.attr("width", $container.width());
			$svg.attr("height", $container.height());
		});

		// Define the drag listeners for drag/drop behaviour of nodes.
		var dragListener = d3.behavior.drag()
			.on("dragstart", function(d) {
				if (d3.event.sourceEvent.which != 1) { // left click only
					return;
				}
				if (d == root) {
					return;
				}
				dragStarted = true;
				nodes = tree.nodes(d);
				d3.event.sourceEvent.stopPropagation();
				// it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
			})
			.on("drag", function(d) {
				if (d3.event.sourceEvent.which != 1) { // left click only
					return;
				}
				if (d == root) {
					return;
				}
				if (dragStarted) {
					domNode = this;
					initiateDrag(d, domNode);
				}

				// get coords of mouseEvent relative to svg container to allow for panning
				relCoords = d3.mouse($('svg').get(0));
				if (relCoords[0] < panBoundary) {
					panTimer = true;
					pan(this, 'left');
				} else if (relCoords[0] > ($('svg').width() - panBoundary)) {

					panTimer = true;
					pan(this, 'right');
				} else if (relCoords[1] < panBoundary) {
					panTimer = true;
					pan(this, 'up');
				} else if (relCoords[1] > ($('svg').height() - panBoundary)) {
					panTimer = true;
					pan(this, 'down');
				} else {
					try {
						clearTimeout(panTimer);
					} catch (e) {

					}
				}

				d.x0 += d3.event.dx;
				d.y0 += d3.event.dy;
				var node = d3.select(this);
				node.attr("transform", "translate(" + d.x0 + "," + d.y0 + ")");
				updateTempConnector();
				updateDeletionSection(true);
			}).on("dragend", function(d) {
				if (d3.event.sourceEvent.which != 1) { // left click only
					return;
				}
				if (d == root) {
					return;
				}
				domNode = this;
				if (selectedNode) {
					// now remove the element from the parent, and insert it into the new elements children
					var index = draggingNode.parent.children.indexOf(draggingNode);
					if (index > -1) {
						draggingNode.parent.children.splice(index, 1);
					}
					// remove the sort order information
					draggingNode.sort_order_hits = null;

					if (typeof selectedNode.children !== 'undefined' || typeof selectedNode._children !== 'undefined') {
						if (typeof selectedNode.children !== 'undefined') {
							selectedNode.children.push(draggingNode);
						} else {
							selectedNode._children.push(draggingNode);
						}
					} else {
						selectedNode.children = [];
						selectedNode.children.push(draggingNode);
					}
					// Make sure that the node being added to is expanded so user can see added node is correctly moved
					expand(selectedNode);
					sortTree();
					endDrag();
				} else if (deletion_selected) { // node deletion
					// remove the element from the parent
					var index = draggingNode.parent.children.indexOf(draggingNode);
					if (index > -1) {
						draggingNode.parent.children.splice(index, 1);
					}
					// hide the dragging node
					svgGroup.selectAll("g.node")
						.data(nodes, function(d) {
							return d.id;
						}).filter(function(d, i) {
							return (d.id == draggingNode.id);
						}).remove();
					sortTree();

					// determine node for centering
					var nodeForCenter = null;
					if (index >= 0 && index < draggingNode.parent.children.length) { // next sibling
						nodeForCenter = draggingNode.parent.children[index];
					} else if ((index - 1) >= 0 && (index - 1) < draggingNode.parent.children.length) { // previous sibling
						nodeForCenter = draggingNode.parent.children[index - 1];
					} else { // parent node
						nodeForCenter = draggingNode.parent;
					}
					endDrag(nodeForCenter);
				} else {
					endDrag();
				}
			});

		function endDrag(nodeForCenter = draggingNode) {
			selectedNode = null;
			d3.select(treeContainerSelector).selectAll('.ghostCircle').attr('class', 'ghostCircle');
			d3.select(domNode).attr('class', 'node');
			// now restore the mouseover event or we won't be able to drag a 2nd time
			d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
			updateTempConnector();
			deletion_selected = false;
			updateDeletionSection(false);
			if (draggingNode !== null) {
				update(root);
				centerNode(nodeForCenter);
				draggingNode = null;
			}
		}

		// Helper functions for collapsing and expanding nodes.
		function collapse(d) {
			if (d.children) {
				d._children = d.children;
				d._children.forEach(collapse);
				d.children = null;
			}
		}

		function expand(d) {
			if (d._children) {
				d.children = d._children;
				d.children.forEach(expand);
				d._children = null;
			}
		}

		var overCircle = function(d) {
			selectedNode = d;
			updateTempConnector();
		};
		var outCircle = function(d) {
			selectedNode = null;
			updateTempConnector();
		};

		// Function to update the temporary connector indicating dragging affiliation
		var updateTempConnector = function() {
			var data = [];
			if (draggingNode !== null && selectedNode !== null) {
				// have to flip the source coordinates since we did this for the existing connectors on the original tree
				data = [{
					source: {
						x: selectedNode.x0,
						y: selectedNode.y0
					},
					target: {
						x: draggingNode.x0,
						y: draggingNode.y0
					}
				}];
			}
			var link = svgGroup.selectAll(".templink").data(data);

			link.enter().append("path")
				.attr("class", "templink")
				.attr("d", d3.svg.diagonal())
				.attr('pointer-events', 'none');

			link.attr("d", d3.svg.diagonal());

			link.exit().remove();
		};

		// update deletion section
		function updateDeletionSection(dragging) {
			if (dragging) {
				deletion_selection.attr("opacity", 0.2)
			} else {
				deletion_selection.attr("opacity", 0);
			}
		}

		// Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.

		function centerNode(source) {
			scale = zoomListener.scale();
			x = -source.x0;
			y = -source.y0;
			x = x * scale + viewerWidth / 2;
			y = y * scale + viewerHeight / 2;
			d3.select(treeContainerSelector).select('g').transition()
				.duration(duration)
				.attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
			zoomListener.scale(scale);
			zoomListener.translate([x, y]);
		}

		// Toggle children function

		function toggleChildren(d) {
			if (d.children) {
				d._children = d.children;
				d.children = null;
			} else if (d._children) {
				d.children = d._children;
				d._children = null;
			}
			return d;
		}

		// Toggle children on click.

		function click(d) {
			if (d3.event.defaultPrevented) return; // click suppressed
			d = toggleChildren(d);
			update(d);
		}

		function update(source) {
			// Compute the new height, function counts total children of root node and sets tree height accordingly.
			// This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
			// This makes the layout more consistent.
			var levelWidth = [1];
			var childCount = function(level, n) {

				if (n.children && n.children.length > 0) {
					if (levelWidth.length <= level + 1) levelWidth.push(0);

					levelWidth[level + 1] += n.children.length;
					n.children.forEach(function(d) {
						childCount(level + 1, d);
					});
				}
			};
			childCount(0, root);
			var newHeight = d3.max(levelWidth) * 35; // 25 pixels per line
			tree = tree.size([newHeight, viewerWidth]);

			// Compute the new tree layout.
			var nodes = tree.nodes(root).reverse(),
				links = tree.links(nodes);

			// Set widths between levels based on maxLabelLength.
			nodes.forEach(function(d) {
				d.y = (d.depth * 100); //maxLabelLength * 10px
				// alternatively to keep a fixed scale one can set a fixed depth per level
				// Normalize for fixed-depth by commenting out below line
				// d.y = (d.depth * 500); //500px per level.
			});

			// Update the nodes…
			node = svgGroup.selectAll("g.node")
				.data(nodes, function(d) {
					return d.id || (d.id = --i);
				});

			// Enter any new nodes at the parent's previous position.
			var nodeEnter = node.enter().append("g")
				//.call(dragListener)
				.attr("class", "node")
				.attr("transform", function(d) {
					return "translate(" + source.x0 + "," + source.y0 + ")";
				});


			// *************************************
			// context menu
			if (authenticated) {
				nodeEnter.on('contextmenu', function(d) {
					createContextMenu(d, d3.select(this));
				});

				function createContextMenu(d, element) {
					d3.event.preventDefault();

					var menu = contextMenu(
						null,
						function() { // do before
							// stop drag and drop mechanism
							element.call(d3.behavior.drag()
								.on('dragstart', function() {
									d3.event.sourceEvent.stopPropagation();
								}));
							element.select(".nodeCircle").classed("context", true);
						},
						function() { // do before handler
							// restore drag and drop mechanism
							element.call(dragListener);
							element.select(".nodeCircle").classed("context", false);
						},
						null
					);

					var treeStoreAndAttach = [{
						text: "Teilbaum für Verschiebung speichern",
						handler: function() {
							storePartTree(d, "partTreeToMove");
						}
					}, {
						text: "Teilbaum als vollständig neuen Thesaurus speichern",
						handler: function() {
							saveAsNewTree(d);
						}
					}];
					var JSON_XMLFunctions = [{
						text: "Teilbaum als JSON exportieren",
						handler: function() {
							exportAsJSON(d);
						}
					}, {
						text: "Teilbaum als XML exportieren",
						handler: function() {
							exportAsXML(d);
						}
					}];

					if (modification_allowed) {
						if (sessionStorage.getItem("partTreeToMove")) {
							treeStoreAndAttach.splice(1, 0, {
								text: "Gespeicherten Teilbaum hier einhängen",
								handler: function() {
									putPartTree(d, "partTreeToMove");
								}
							});
						}
						JSON_XMLFunctions.push({
							text: "JSON importieren und Teilbaum hier einhängen",
							handler: function() {
								importJSONFileAndAttach(d);
							}
						}, {
							text: "XML importieren und Teilbaum hier einhängen",
							handler: function() {
								importXMLFileAndAttach(d);
							}
						});
						menu.items({
							text: "Neues Konzept einfügen",
							submenu: [{
								text: "Konzept einfügen",
								handler: function() {
									createNewNode(d);
								}
							}, {
								text: "Konzept einfügen und Detailansicht öffnen",
								handler: function() {
									createNewNode(d, true);
								}
							}, ]
						});
						// change sort order of siblings
						if (d.parent && d.parent.children.length > 1) {
							menu.items({
								text: "Konzepte ordnen",
								submenu: [{
									text: "Konzepte alphabetisch anordnen",
									handler: function() {
										changePositionMenu(d, "alphabetical");
									}
								}, {
									text: "\u2190 \u2192 Konzepte ausrichten",
									handler: function() {
										createPositionMenu(d, element);
									}
								}]
							});
						}
					}

					menu.items({
						text: "Teilbaum speichern und wiederherstellen",
						submenu: treeStoreAndAttach
					}, {
						text: "JSON/XML-Datei importieren und exportieren",
						submenu: JSON_XMLFunctions
					});

					menu(element);
				}

				// function for saving the selected part tree as completely new tree
				function saveAsNewTree(d) {
					function moveForward() {
						var url = sourceJSON.match(/(.+\/)[0-9]+\/tree_data[\/]?/)[1];
						var urlForPostRequest = url + "vocab_create/";
						var jsonString = JSON.stringify(traverseTreeAndReplaceID(d, true));
						var submitForm = $("<form method='post' action='" + urlForPostRequest + "' />").hide().appendTo(document.body);
						$("input[name=csrfmiddlewaretoken]").appendTo(submitForm);
						$("<input />")
							.attr("type", "hidden")
							.attr("name", "tree")
							.attr("value", jsonString)
							.appendTo(submitForm);
						submitForm.submit();
					}
					if (modification_allowed) {
						getConfirmationBoxForSavingTree(moveForward);
					} else {
						moveForward();
					}
				}

				// export the part tree as JSON
				function exportAsJSON(d) {
					var fileName = proposeFilename("json_export");
					var blob = new Blob([JSON.stringify(traverseTree(d), null, 4)], {
						type: "application/json"
					});
					saveAs(blob, fileName + ".json");
				}

				// export the part tree as XML
				function exportAsXML(d) {
					var fileName = proposeFilename("xml_export");
					var thesaurusToExport = {
						thesaurus: traverseTree(d)
					};
					var blob = new Blob([(new X2JS()).json2xml_str(thesaurusToExport)], {
						type: "text/xml"
					});
					saveAs(blob, fileName + ".xml");
				}

				// propose a filename
				function proposeFilename(defaultName) {
					var selectedVocab = $("select#vocab_selection").find("option:selected");
					if (selectedVocab.length == 0) {
						return defaultName;
					} else {
						return (selectedVocab.data("vocab").name + "_" + selectedVocab.data("vocab").version).replace(/[^0-9a-zA-Z_]+/g, "_");
					}
				}

				// import a tree as JSON file and attach this tree to an existing one
				function importJSONFileAndAttach(d) {
					selectJSONFileAndValidate(function(json) {
						attachJSONtoTree(json, d);
					});
				}

				// import a tree as XML file and attach this tree to an existing one
				function importXMLFileAndAttach(d) {
					selectXMLFileAndValidate(function(json) {
						attachJSONtoTree(json, d);
					});
				}

				// helper function for attaching JSON data to the tree
				function attachJSONtoTree(json, d) {
					if (d._children) {
						expand(d);
					}
					if (!d.children) {
						d.children = [];
					}
					d.children.push(traverseTreeAndReplaceID(json));
					update(root);
					centerNode(d);
				}

				// store this part of the tree for moving it to another place
				function storePartTree(d, key) {
					sessionStorage.setItem(key, JSON.stringify(traverseTreeAndReplaceID(d)));
				}

				// attach the stored part of the tree to this node
				function putPartTree(d, key) {
					if (d._children) {
						expand(d)
					}
					if (!d.children) {
						d.children = [];
					}
					d.children.push(JSON.parse(sessionStorage.getItem(key)));
					sessionStorage.removeItem(key);
					update(root);
					centerNode(d);
				}

				// create menu for positioning the node among his siblings
				function createPositionMenu(d, element) {
					var menu = contextMenu({
							'rect': {
								'mouseout': {
									'fill': '#ABEBC6'
								},
								'mouseover': {
									'fill': '#008000'
								}
							},
							'text': {
								'fill': '#003300',
								'font-size': '16'
							}
						},
						function() { // do before
							// stop drag and drop mechanism
							element.call(d3.behavior.drag()
								.on('dragstart', function() {
									d3.event.sourceEvent.stopPropagation();
								}));
							element.select(".nodeCircle").classed("move", true);
						},
						function() { // do before handler
							// restore drag and drop mechanism
							element.call(dragListener);
							element.select(".nodeCircle").classed("move", false);
						},
						null
					).items({
						text: "\u00A0\u00A0\u00A0\u2718\u00A0\u00A0\u00A0",
						handler: function() {},
						position: "bottom"
					});
					// excluding first child
					if (d.parent.children.indexOf(d) > 0) {
						menu.items({
							text: "\u2190\u00A0\u00A0\u00A0\u00A0\u00A0",
							handler: function() {
								changePositionMenu(d, "left");
								createPositionMenu(d, element);
							},
							position: "left"
						}, {
							text: "\u21E4\u00A0\u00A0\u00A0\u00A0\u00A0",
							handler: function() {
								changePositionMenu(d, "leftEnd");
								createPositionMenu(d, element);
							},
							position: "left"
						});
					}
					// excluding last child
					if (d.parent.children.indexOf(d) < (d.parent.children.length - 1)) {
						menu.items({
							text: "\u00A0\u00A0\u00A0\u00A0\u00A0\u2192",
							handler: function() {
								changePositionMenu(d, "right");
								createPositionMenu(d, element);
							},
							position: "right"
						}, {
							text: "\u00A0\u00A0\u00A0\u00A0\u00A0\u21E5",
							handler: function() {
								changePositionMenu(d, "rightEnd");
								createPositionMenu(d, element);
							},
							position: "right"
						});
					}
					menu(element, true);
				}

				// Function for changing the sort order of the siblings
				function changePositionMenu(d, sortOrder) {
					var children = d.parent.children;
					if (sortOrder == "alphabetical") {
						for (var i = 0; i < children.length; i++) {
							children[i].sort_order_hits = null;
						}
					} else if (["left", "leftEnd", "right", "rightEnd"].includes(sortOrder)) {
						for (var i = 0; i < children.length; i++) {
							if (children[i].sort_order_hits) {
								children[i].sort_order_hits[0]["position_hit"] = i;
							} else {
								children[i].sort_order_hits = [{
									id: -1,
									position_hit: i
								}];
							}
						}
						switch (sortOrder) {
							case "left":
								swapSiblings(d, children, -1);
								break;
							case "right":
								swapSiblings(d, children, 1);
								break;
							case "leftEnd":
								moveToEnd(d, children, true);
								break;
							case "rightEnd":
								moveToEnd(d, children, false);
								break;
						}

						function swapSiblings(d, children, siblingOffset) {
							var thisNodePositionHit = d.sort_order_hits[0]["position_hit"];
							var sibling = children[children.indexOf(d) + siblingOffset];
							d.sort_order_hits[0]["position_hit"] = sibling.sort_order_hits[0]["position_hit"];
							sibling.sort_order_hits[0]["position_hit"] = thisNodePositionHit;
						}

						function moveToEnd(d, children, leftEnd) {
							var found = false;
							for (var i = 0; i < children.length; i++) {
								if (children[i] == d) {
									found = true;
									if (leftEnd) {
										children[i].sort_order_hits[0]["position_hit"] = 0;
									} else {
										children[i].sort_order_hits[0]["position_hit"] = children.length - 1;
									}
								} else if (!found) {
									if (leftEnd) {
										children[i].sort_order_hits[0]["position_hit"] = i + 1;
									} else {
										children[i].sort_order_hits[0]["position_hit"] = i;
									}
								} else {
									if (leftEnd) {
										children[i].sort_order_hits[0]["position_hit"] = i;
									} else {
										children[i].sort_order_hits[0]["position_hit"] = i - 1;
									}
								}
							}
						}
					}
					update(root);
					centerNode(d);
				}
			}
			// end context menu
			// *************************************


			if (modification_allowed) {
				nodeEnter.call(dragListener);
			}

			var nodeEnterCircle = nodeEnter.append("circle")
				.attr('class', 'nodeCircle')
				.attr("r", 0)
				.style("fill", function(d) {
					return d._children ? "lightsteelblue" : "#fff";
				});

			if (completeMode) {
				nodeEnterCircle.on('click', click);
			} else { // reduced mode
				if (referencedConceptSelectionAllowed) {
					nodeEnterCircle.on('click', function(d) {
						svgGroup.selectAll("circle.nodeCircle")
							.classed("selected", false);
						d3.select(this)
							.classed("selected", true);
						// update reference of main tree
						reference["target_concept_id"] = d.id;
					});
				}
			}

			var nodeEnterText = nodeEnter.append("text")
				.attr('class', 'nodeText')
				.attr("text-anchor", function(d) {
					return d.children || d._children ? "end" : "start";
				})
				.attr("transform", function(d) {
					return d.children || d._children ? "translate(0,-8) rotate(40)" : "translate(-5,12) rotate(40)";
				})
				.text(function(d) {
					return getNodeLabel(d);
				})
				.style("fill-opacity", 0);

			/* show detail view for node attributes */
			if (completeMode) {
				nodeEnterText.on("click", function(d) {
					var attributeContainerList = [
						// labels section
						new AttributeContainer(
							"Labels",
							"labels",
							function(d) {
								return d["text"];
							}, [{
								name: "text"
							}, {
								name: "lang",
								selection: languages,
								defaultValue: get_selected_language()
							}, {
								name: "is_preferred",
								selection: ["true", "false"]
							}],
							true,
							// descendant
							[
								new AttributeContainer(
									"Varianten",
									"variants",
									function(d) {
										return d["text"];
									}, [{
										name: "text"
									}, {
										name: "comment",
										defaultValue: ""
									}]
								)
							]
						),
						// definitions section
						new AttributeContainer(
							"Definitionen",
							"definitions",
							function(d) {
								return d["text"];
							}, [{
								name: "text"
							}, {
								name: "lang",
								selection: languages,
								defaultValue: get_selected_language()
							}]
						),
						// external references section
						new AttributeContainer(
							"Externe Verlinkungen",
							"external_references",
							function(d) {
								return d["target_url"];
							}, [{
								name: "target_url",
								defaultValue: "http://",
								preprocess: function(value) {
									return value.prop("href");
								},
								postprocess: function(value) {
									return $("<a target='_blank' />").prop("href", value).text(value);
								}
							}, ]
						)
					];

					getDetailView(this, d, getNodeLabel(d), attributeContainerList);
				});

			}

			if (modification_allowed) { // stop drag and drop for labels
				nodeEnterText.call(d3.behavior.drag()
					.on('dragstart', function() {
						d3.event.sourceEvent.stopPropagation();
					}));
			}

			// phantom node to give us mouseover in a radius around it
			nodeEnter.append("circle")
				.attr('class', 'ghostCircle')
				.attr("r", 12)
				.attr("opacity", 0.2) // change this to zero to hide the target area
				.style("fill", "red")
				.attr('pointer-events', 'mouseover')
				.on("mouseover", function(node) {
					overCircle(node);
				})
				.on("mouseout", function(node) {
					outCircle(node);
				});

			// Update the text to reflect whether node has children or not.
			node.select('text')
				/*.attr("x", function(d) {
				    return d.children || d._children ? -10 : 10;
				})*/
				.attr("text-anchor", function(d) {
					return d.children || d._children ? "end" : "start";
				})
				.attr("transform", function(d) {
					return d.children || d._children ? "translate(0,-8) rotate(40)" : "translate(-5,12) rotate(40)";
				})
				.text(function(d) {
					return getNodeLabel(d);
				});

			// Change the circle fill depending on whether it has children and is collapsed
			node.select("circle.nodeCircle")
				.attr("r", 4.5)
				.style("fill", function(d) {
					return d._children ? "lightsteelblue" : "#fff";
				});

			// Transition nodes to their new position.
			var nodeUpdate = node.transition()
				.duration(duration)
				.attr("transform", function(d) {
					return "translate(" + d.x + "," + d.y + ")";
				});

			// Fade the text in
			nodeUpdate.select("text")
				.style("fill-opacity", 1);

			// Transition exiting nodes to the parent's new position.
			var nodeExit = node.exit().transition()
				.duration(duration)
				.attr("transform", function(d) {
					return "translate(" + source.x + "," + source.y + ")";
				})
				.remove();

			nodeExit.select("circle")
				.attr("r", 0);

			nodeExit.select("text")
				.style("fill-opacity", 0);

			// Update the links…
			var link = svgGroup.selectAll("path.link")
				.data(links, function(d) {
					return d.target.id;
				});

			// Enter any new links at the parent's previous position.
			link.enter().insert("path", "g")
				.attr("class", "link")
				.attr("d", function(d) {
					var o = {
						x: source.x0,
						y: source.y0
					};
					return diagonal({
						source: o,
						target: o
					});
				});

			// Transition links to their new position.
			link.transition()
				.duration(duration)
				.attr("d", diagonal);

			// Transition exiting nodes to the parent's new position.
			link.exit().transition()
				.duration(duration)
				.attr("d", function(d) {
					var o = {
						x: source.x,
						y: source.y
					};
					return diagonal({
						source: o,
						target: o
					});
				})
				.remove();

			// Stash the old positions for transition.
			nodes.forEach(function(d) {
				d.x0 = d.x;
				d.y0 = d.y;
			});
		}

		// Append a group which holds all nodes and which the zoom Listener can act upon.
		var svgGroup = baseSvg.append("g");

		// Define the root
		root = treeData;
		if (completeMode) {
			rootForSaving = root; // copy for saving
		}
		root.x0 = viewerWidth / 2;
		root.y0 = 0;

		// Layout the tree initially and center on the root node.
		update(root);
		centerNode(root);



		if (modification_allowed) {
			// selection to delete a node
			var deletion_selected = false;
			var deletion_selection = baseSvg.append("circle")
				.attr("cx", 30)
				.attr("cy", 40)
				.attr("r", 40)
				.attr("opacity", 0)
				.attr("fill", "red");

			baseSvg.append("path")
				.attr("d", "M 5,20 L 35,20")
				.style("stroke", "black")
				.style("stroke-width", 3)
				.attr("transform", "translate(10, 20)");

			baseSvg.append("circle")
				.attr("cx", 30)
				.attr("cy", 40)
				.attr("r", 20)
				.attr("opacity", 0.3)
				.attr("fill", "red")
				.on("mouseover", function(node) {
					d3.select(this).attr("opacity", 0.7);
					deletion_selected = true;
				})
				.on("mouseout", function(node) {
					d3.select(this).attr("opacity", 0.3);
					deletion_selected = false;
				});

			// selection to insert a new node
			baseSvg.append("path")
				.attr("d", "M 20,5 L 20,35 M 5,20 L 35,20")
				.style("stroke", "black")
				.style("stroke-width", 3)
				.attr("transform", "translate(10, 80)");

			var insert_selection = baseSvg.append("circle")
				.attr("cx", 30)
				.attr("cy", 100)
				.attr("r", 20)
				.attr("opacity", 0.5)
				.attr("fill", "green")
				.on("mousedown", function(d) {
					d3.select(this).attr("opacity", 1);
					createNewNode(root);
				})
				.on("mouseup", function(d) {
					d3.select(this).attr("opacity", 0.5);
				});

			// attach a new node as child node of the root
			function createNewNode(node, openDetailView = false) {
				if (node._children) {
					expand(node)
				}
				if (!node.children) {
					node.children = [];
				}
				var newNode = {
					labels: [{
						id: -1,
						text: "[New Node]",
						lang: get_selected_language(),
						is_preferred: "true"
					}]
				};
				node.children.push(newNode);
				update(root);
				var newTextNode = svgGroup.selectAll("g.node")
					.data(tree.nodes(root), function(d) {
						return d.id;
					})
					.filter(function(d, i) {
						return (d.id == newNode.id);
					})
					.select("text")
					.style("fill", "red")
					.style("font-size", "12px");
				centerNode(newNode);
				if (openDetailView) {
					newTextNode.each(function(d, i) {
						var onClickFunc = d3.select(this).on("click");
						onClickFunc.apply(this, [d, i]);
						var editButton = $(".modal-content div.labels button.edit").first();
						editButton.click();
						editButton.prev().select();
					});
				}
			}
		}

		if (reference && reference["target_concept_id"]) {
			focusOnSelectedNode(reference["target_concept_id"]);
		}

		if (completeMode) {
			finalization(switches, load_status, "Daten sind geladen", false);
		}




		/* ************************* */
		/* detail view for each node */
		// constructor function for storing the attributes and creating the corresponding input fields
		function AttributeContainer(title, key, getShortString, attributes, restoreDefaultIfEmpty = false, descendant = null) {
			this.title = title;
			this.key = key;
			this.getShortString = getShortString;
			this.restoreDefaultIfEmpty = restoreDefaultIfEmpty;
			this.descendant = descendant;
			this.attributeDict = {};
			for (var i = 0; i < attributes.length; i++) {
				this.attributeDict[attributes[i].name] = attributes[i];
			}
			this.getAttributeNames = function() {
				var attributeNames = [];
				for (var attributeName in this.attributeDict) {
					attributeNames.push(attributeName);
				}
				return attributeNames;
			};
			this.getDefaultValue = function(attributeName) {
				var attribute = this._findAttributeByName(attributeName);
				if (attribute) {
					if (attribute.defaultValue != null) {
						return attribute.defaultValue;
					} else if (attribute.selection) {
						return attribute.selection[0];
					} else {
						return attribute.name.toUpperCase();
					}
				} else {
					return null;
				}
			};
			this.getDefaultTextBox = function(attributeName, fieldToInsert) {
				var attribute = this._findAttributeByName(attributeName);
				if (attribute) {
					var defaultValue = this._getProcessedValue(attribute.postprocess, this.getDefaultValue(attributeName));
					if (defaultValue) {
						if (defaultValue instanceof jQuery) {
							fieldToInsert.empty();
							fieldToInsert.append(defaultValue);
						} else {
							fieldToInsert.text(defaultValue);
						}
					}
				}
			}
			this.getInputFieldObject = function(attributeName, content) {
				var attribute = this._findAttributeByName(attributeName);
				if (attribute) {
					if (content.children().length == 0) {
						var realContent = content.text();
					} else {
						var realContent = content.children().first();
					}
					var valueToSet = this._getProcessedValue(attribute.preprocess, realContent)
					if (attribute.selection) {
						var options = "";
						for (var i = 0; i < attribute.selection.length; i++) {
							options += "<option value='" + attribute.selection[i] + "'";
							if (attribute.selection[i] == valueToSet) {
								options += "selected='selected'";
							}
							options += ">" + attribute.selection[i] + "</option>";
						}
						return $("<select>" + options + "</select>");
					} else {
						return $("<textarea rows='" + Math.ceil(content.parent().height() / 20) + "' />").text(valueToSet);
					}
				} else {
					return null;
				}
			};
			this.insertNewValue = function(attributeName, valueToSet, fieldToInsert) {
				var attribute = this._findAttributeByName(attributeName);
				if (attribute) {
					var newValue = this._getProcessedValue(attribute.postprocess, valueToSet);
					if (newValue instanceof jQuery) {
						fieldToInsert.empty();
						fieldToInsert.append(newValue);
					} else {
						fieldToInsert.text(newValue);
					}
				}
			}
			this.insertNewValueFromInputFieldObject = function(attributeName, inputField, fieldToInsert) {
				var attribute = this._findAttributeByName(attributeName);
				if (attribute) {
					var enteredValue = inputField.val().trim();
					this.insertNewValue(attributeName, enteredValue, fieldToInsert);
					return enteredValue;
				} else {
					return null;
				}
			}
			this._findAttributeByName = function(attributeName) {
				return this.attributeDict[attributeName];
			};
			this._getProcessedValue = function(func, value) {
				if (func) {
					return func(value);
				} else {
					return value;
				}
			}
		}

		// main function for creating a modal detail view
		function getDetailView(nodeText, d, title, attributeContainerList) {
			var myModal = $("<div class='modal node-detail'/>").appendTo(document.body);
			var closeCross = $("<span class='close'>&times;</span>");
			var modalContent = $("<div class='modal-content' />").append(closeCross);
			closeCross.on("click", function (event) {
				closeModal();
			});
			// create sections
			modalContent.append("<h1>" + title + "</h1>");

			// if this modal window is the first one
			if ($("div.modal.node-detail").length == 1) {
				// ******************************
				/* linked trees */
				var references_div = $("<div class='references' />").appendTo(modalContent);
				references_div.append("<h4>Verlinkte Konzepte</h4>");
				var linkedTreeSection = $("<div class='linkedTreeSection' />").appendTo(references_div);
				references_div.append("<h4>Folgende Verlinkungen zeigen auf dieses Konzept</h4>");
				var reverseLinkedTreeSection = $("<div class='reverseLinkedTreeSection' />").appendTo(references_div);

				// construct a dictionary containing all vocabularies
				var vocab_dict = {};
				for (var i = 0; i < vocabs.length; i++) {
					vocab_dict[vocabs[i].id] = vocabs[i];
				}

				// references to linked tree
				function contructReferences(referenceKey, container, reverse) {
					if (d[referenceKey]) {
						sortEntries(d[referenceKey]);
						for (var i = 0; i < d[referenceKey].length; i++) {
							var vocab_id = d[referenceKey][i]["target_vocab_id"];
							if (vocab_dict[vocab_id]) {
								var treeBox = $("<div class='treebox' />").appendTo(container);
								var treeContainerID = container.attr("class") + d[referenceKey][i]["id"];
								var treeContainer = $("<div class='treecontainer' id='" + treeContainerID + "' />").appendTo(treeBox);
								var rootURL = window.location.href.match(/(.+\/TM\/)tree\//)[1];
								constructTree(rootURL + vocab_id + "/tree_data/", ("#" + treeContainerID), null, null, null, language_selection, d[referenceKey][i], modification_allowed && !reverse);
								constructLinkedTreeButton(treeBox, rootURL, vocab_dict[vocab_id], d[referenceKey], d[referenceKey][i], reverse);
							}
						}
					}
					createDropDownForTreeInsert(container, reverse);
				}

				contructReferences("references", linkedTreeSection, false);
				contructReferences("reverse_references", reverseLinkedTreeSection, true);

				// construct drop-down list for inserting new linked trees
				function createDropDownForTreeInsert(container, read_only) {
					if (modification_allowed && !read_only) {
						var vocab_select = getVocabSelection("linked_tree_selection");
						vocab_select.prepend($("<option value=''>Neue Verlinkung einfügen...</option>"));
						vocab_select.val('');
						vocab_select.on("change", function () {
							if (this.value) {
								if (d["references"]) {
									var LowestID = d3.min(d["references"], function (d) {
										return d.id;
									});
									var newID = (LowestID < 0) ? (LowestID - 1) : -1;
								} else {
									var newID = -1;
								}
								var target_vocab = $(this).find(":selected").data("vocab");
								var newReference = {
									id: newID,
									target_concept_id: null,
									target_vocab_id: target_vocab.id,
								}
								var treeBox = $("<div class='treebox' />").insertBefore(vocab_select);
								var treeContainerID = container.attr("class") + newReference.id;
								var treeContainer = $("<div class='treecontainer' id='" + treeContainerID + "' />").appendTo(treeBox);
								if (d["references"] == null) {
									d["references"] = [];
								}
								d["references"].push(newReference);
								constructTree($(this).find("option:selected").data("url"), ("#" + treeContainerID), null, null, null, language_selection, newReference, modification_allowed);
								var rootURL = window.location.href.match(/(.+\/TM\/)tree\//)[1];
								constructLinkedTreeButton(treeBox, rootURL, target_vocab, d["references"], newReference);
								vocab_select.val('');
							}
						});
						container.append(vocab_select);
					}
				}

				// construct linked tree delete and change buttons
				function constructLinkedTreeButton(treeBox, rootURL, referenced_vocab, references, referenceToDelete, read_only = false) {
					// change to a linked tree
					$("<button class='tree_change' >Wechseln zu: <span class='vocabulary'>" + referenced_vocab["name"] + "</span></button>").appendTo(treeBox)
						.on("click", function (event) {
							closeModal();  // for clean up functions
							function _callback() {
								window.loadOtherVocabulary(referenced_vocab.id);
							}

							if (modification_allowed) {
								getConfirmationBoxForSavingTree(_callback);
							} else {
								_callback();
							}
						});
					if (modification_allowed && !read_only) {
						// delete reference to linked tree
						$("<span class='delete_reference'>&ominus;</span>").appendTo(treeBox)
							.on(
								"click", {
									treeBox: treeBox,
									references: references,
									referenceToDelete: referenceToDelete

								},
								function (event) {
									event.data.treeBox.remove();
									var index = event.data.references.indexOf(event.data.referenceToDelete);
									event.data.references.splice(index, 1);
								}
							);
					}
				}

				// clean up: delete empty references
				function cleanUpReferences(references) {
					if (references) {
						var toDelete = [];
						for (var i = 0; i < references.length; i++) {
							if (!references[i]["target_concept_id"]) {
								toDelete.push(references[i]);
							}
						}
						for (var i = 0; i < toDelete.length; i++) {
							var index = references.indexOf(toDelete[i]);
							references.splice(index, 1);
						}
					}
				}
				/* linked trees */
				// ****************************

			}


			for (var i = 0; i < attributeContainerList.length; i++) {
				modalContent.append(getDetailSection(d, attributeContainerList[i]));
			}
			myModal.append(modalContent);


			// When the user clicks anywhere outside of the modal, close it
			var removeModal = function (event) {
				if (event.target == myModal.get(0)) {
					$(window).off("click", removeModal);
					closeModal();
				}
			}
			$(window).on("click", removeModal);

			// function called when modal is closed
			function closeModal() {
				myModal.remove();
				setPlaceHolder(d, attributeContainerList);
				// final operations are executedq only if this is the last modal view
				if ($("div.modal.node-detail").length == 0) {
					if ("function" === typeof(cleanUpReferences)) {
						cleanUpReferences(d["references"]);
					}
					if (nodeText) {
						d3.select(nodeText).style("fill", null).style("font-size", null);
					}
					update(d);
				}
			}

			// set a placeholder if the last record was removed
			function setPlaceHolder(d, attributeContainerList) {
				for (var i = 0; i < attributeContainerList.length; i++) {
					if (attributeContainerList[i].restoreDefaultIfEmpty) {
						var key = attributeContainerList[i].key;
						if (d[key].length == 0) {
							var attributes = attributeContainerList[i].getAttributeNames();
							var newData = {
								id: -1
							};
							for (var j = 0; j < attributes.length; j++) {
								newData[attributes[j]] = attributeContainerList[i].getDefaultValue(attributes[j]);
							}
							d[key].push(newData);
						}
					}
				}
			}


			// constructs an editable section for the given attributes
			function getDetailSection(d, attributeContainer) {
				var section = $("<div class='" + attributeContainer.key + "' />");
				section.append($("<h4/>").text(attributeContainer.title));
				var attributes = attributeContainer.getAttributeNames();
				// table construction
				var table = $("<table class='" + attributeContainer.key + " hide-first-col' />").appendTo(section);
				// table header
				var row = $("<tr />").appendTo(table);
				$("<th />").appendTo(row);
				for (var i = 0; i < attributes.length; i++) {
					var cell = $("<th>" + attributes[i] + "</th>").appendTo(row);
				}

				// descendant section
				if (attributeContainer.descendant) {
					var cell = $("<th />").appendTo(row);
					cell.append(attributeContainer.descendant.title);
				}

				// table body
				if (d[attributeContainer.key]) {
					sortEntries(d[attributeContainer.key]);
					for (var i = 0; i < d[attributeContainer.key].length; i++) {
						var row = $("<tr />").appendTo(table);
						var cell = $("<td />").appendTo(row);
						// hidden field with ID
						cell.append($("<input type='hidden' value='" + d[attributeContainer.key][i].id + "' />"));
						for (var j = 0; j < attributes.length; j++) {
							var cell = $("<td />").appendTo(row);
							var fieldToInsert = $("<span class='" + attributes[j] + "'></span>").appendTo(cell);
							attributeContainer.insertNewValue(attributes[j], d[attributeContainer.key][i][attributes[j]], fieldToInsert);
							if (modification_allowed) {
								cell.append($("<button class='edit'>Edit</button>"));
							}
						}

						// descendant section
						if (attributeContainer.descendant) {
							var cell = $("<td />").appendTo(row);
							cell.append(getDescendantSection(d[attributeContainer.key][i], attributeContainer.getShortString, attributeContainer.descendant));
						}

						if (modification_allowed) {
							// row delete button
							var deleteButton = $("<span class='delete'>&ominus;</span>").appendTo(row);
						}
					}
				}

				if (modification_allowed) {
					// insert button for new entries
					var insertButton = $("<span class='insert'>&oplus;</span>").appendTo(section);
					insertButton.on("click", function (event) {
						var newData = {
							id: (findLowestID(table) - 1)
						};
						if (d[attributeContainer.key]) {
							d[attributeContainer.key].push(newData);
						} else {
							d[attributeContainer.key] = [newData];
						}
						var row = $("<tr />").appendTo(table);
						var cell = $("<td />").appendTo(row);
						cell.append($("<input type='hidden' value='" + newData.id + "' />"));
						for (var j = 0; j < attributes.length; j++) {
							newData[attributes[j]] = attributeContainer.getDefaultValue(attributes[j]);
							var cell = $("<td />").appendTo(row);
							var fieldToInsert = $("<span class='" + attributes[j] + "'></span>").appendTo(cell);
							attributeContainer.getDefaultTextBox(attributes[j], fieldToInsert);
							cell.append($("<button class='edit'>Edit</button>"));
						}

						// descendant section
						if (attributeContainer.descendant) {
							var cell = $("<td />").appendTo(row);
							cell.append(getDescendantSection(newData, attributeContainer.getShortString, attributeContainer.descendant));
						}

						// row delete button
						var deleteButton = $("<span class='delete'>&ominus;</span>").appendTo(row);
					});


					// edit function
					section.on("click", "button.edit", function (event) {
						var button = $(event.target);
						button.hide();
						var content = button.prev();
						var editField = attributeContainer.getInputFieldObject(content.attr("class"), content);
						content.hide();
						editField.insertAfter(content);
						editField.focus();
						editField.on("focusout", function (event) {
							var newValue = attributeContainer.insertNewValueFromInputFieldObject(content.attr("class"), editField, content);
							if (!newValue) {
								attributeContainer.getDefaultTextBox(content.attr("class"), content);
								newValue = attributeContainer.getDefaultValue(content.attr("class"));
							}
							var recordToChange = findRecordWithID(attributeContainer.key, d, button.closest("tr").find("input[type='hidden']").val());
							recordToChange[content.attr("class")] = newValue;
							content.show();
							editField.remove();
							button.show();
						});
					});

					// delete function
					section.on("click", "span.delete", function (event) {
						$(this).parent().remove();
						var recordToDelete = findRecordWithID(attributeContainer.key, d, $(this).closest("tr").find("input[type='hidden']").val());
						var indexOfRecordToDelete = d[attributeContainer.key].indexOf(recordToDelete);
						d[attributeContainer.key].splice(indexOfRecordToDelete, 1);
					});
				}


				// helper function
				function findRecordWithID(key, d, searchedID) {
					for (var i = 0; i < d[key].length; i++) {
						if (d[key][i].id == searchedID) {
							return d[key][i];
						}
					}
					return null;
				}

				// helper function
				function findLowestID(table) {
					var lowest = 0;
					var idHolders = table.find("input[type='hidden']");
					for (var i = 0; i < idHolders.length; i++) {
						var givenID = +(idHolders[i].value);
						if (givenID < lowest) {
							lowest = givenID;
						}
					}
					return lowest;
				}

				return section;
			}

			// helper function for consistent sorting
			function sortEntries(entries) {
				entries.sort(function (a, b) {
					if (a.id > 0 && b.id > 0) {
						return a.id - b.id;
					} else if (a.id <= 0 && b.id <= 0) {
						return Math.abs(a.id) - Math.abs(b.id);
					} else if (a.id <= 0) {
						return 1;
					} else {
						return -1;
					}
				});
			}

			// descendant sections
			function getDescendantSection(d, getShortString, attributeContainerList) {
				var titles = [];
				for (var i = 0; i < attributeContainerList.length; i++) {
					titles.push(attributeContainerList[i].title);
				}
				var button = $("<button class='descendant'>" + titles.join(" / ") + "</button>");
				button.on("click", function (event) {
					getDetailView(null, d, getShortString(d), attributeContainerList);
				});
				return button;
			}

		}

		/* end detail view */
		/* ************************* */





		/* *********************************** */
		/* label and language */

		if (completeMode) {
			// update tree after language change
			language_selection.off("change");
			language_selection.on("change", function () {
				CookieManager.setItem("preferred_language", $(this).val());
				update(root);
				resetSearch();
			});
		}

		function get_selected_language() {
			return language_selection.val();
		}


		// function for getting the node label to display
		function getNodeLabel(node) {
			var label = findLabelInBrowserLanguage(node);
			return (label ? label.text : "");
		}

		// function for finding the label in the appropriate language
		function findLabelInBrowserLanguage(node) {
			if (node.labels && node.labels.length > 0) {
				var labelsInBrowserLanguage = findLabelsOfAppropriateLanguage(node);
				labelsInBrowserLanguage.sort(function (a, b) {
					// Sort by is_preferred attribute, or of equal by id
					return (sortByIsPreferred(a, b) || (a.id - b.id));
				});
				return labelsInBrowserLanguage[0];
			}
			return null;
		}

		// find labels in appropriate language
		function findLabelsOfAppropriateLanguage(node) {
			var labelsInBrowserLanguage = findLabelInSpecificLanguage(node, get_selected_language());
			if (labelsInBrowserLanguage.length == 0) {
				labelsInBrowserLanguage = findLabelInSpecificLanguage(node, "en");
			}
			if (labelsInBrowserLanguage.length == 0) {
				labelsInBrowserLanguage = node.labels.slice(0);
			}
			return labelsInBrowserLanguage;
		}

		// Helper function: sort by the is_preferred attribute
		function sortByIsPreferred(a, b) {
			// Helper function transform string to boolean
			function toBoolean(str) {
				return (str == "true");
			}

			return toBoolean(b.is_preferred) - toBoolean(a.is_preferred);
		}

		// function for finding labels with a specific language
		function findLabelInSpecificLanguage(node, lang) {
			var labelsInBrowserLanguage = [];
			for (var n = 0; n < node.labels.length; n++) {
				if (node.labels[n].lang == lang) {
					labelsInBrowserLanguage.push(node.labels[n]);
				}
			}
			return labelsInBrowserLanguage;
		}

		/* end label and language */
		/* *********************************** */






		/* *************************** */
		/* search and filter section */

		if (completeMode) {

			// search method
			var searchMethods = {
				startsWith: {
					name: "beginnt mit",
					func: function (a, b) {
						return a.startsWith(b);
					},
				},
				endsWith: {
					name: "endet auf",
					func: function (a, b) {
						return a.endsWith(b);
					},
				},
				contains: {
					name: "enthält",
					func: function (a, b) {
						return a.includes(b);
					}
				},
				exact: {
					name: "ist gleich",
					func: function (a, b) {
						return a == b;
					}
				}
			};
			var searchMethodSelect = $("<select id='search_method'><select>").appendTo(search_section);
			for (var searchMethod in searchMethods) {
				searchMethodSelect.append(
					$("<option/>")
						.text(searchMethods[searchMethod].name)
						.data("method", searchMethods[searchMethod].func));
			}
			searchMethodSelect.on("change", function (event) {
				resetSearch();
			});

			// search field
			var search_field = $("<input type='search' id='search_term'/>").appendTo(search_section);
			search_field.on("focus", function () {
				resetSearch();
			});
			// search button
			var search_button = $("<button id='search_start'>Suchen</button>").appendTo(search_section);

			// Button to expand tree
			$("<button id='expand'>Expandieren</button>").appendTo(search_section)
				.on("click", function (event) {
					expandRecursive(root);
					update(root);
					centerNode(root);
				});

			var $resultsSection = $('<div id="search_results"/>').hide().appendTo(search_section);
			var hit_counter = $("<p id='hit-counter'/>").appendTo($resultsSection);
			var found = null;
			var search_result_number = null;
			search_button.on("click", function () {
				var $leftArrow, $rightArrow;
				resetSearch();

				if (search_field.val().trim()) {
					var search_term = search_field.val().toLowerCase().trim();
					storeSearchTermInAutocompleteCache(search_term);
					found = [];
					search(root, search_term);
					search_result_number = null;
				}
				if (found && 0 < found.length) {
					$(this).prop("disabled", true);
					// select first hit
					search_result_number = 0;
					show_search_result(found, hit_counter);

					$leftArrow = $("<button class='search-arrow'>&#8592;</button>").prependTo($resultsSection);
					$leftArrow.on("click", function (event) {
						disableElementTemporarily($rightArrow.add($leftArrow), 100);
						if (search_result_number == null) {
							search_result_number = found.length - 1;
						} else {
							search_result_number--;
							if (search_result_number < 0) {
								search_result_number = found.length - 1;
							}
						}
						show_search_result(found, hit_counter);
					});

					$rightArrow = $("<button class='search-arrow'>&#8594;</button>").insertAfter($leftArrow);
					$rightArrow.on("click", function (event) {
						disableElementTemporarily($rightArrow.add($leftArrow), 100);
						if (search_result_number == null) {
							search_result_number = 0;
						} else {
							search_result_number = ++search_result_number % found.length;
						}
						show_search_result(found, hit_counter);
					});

					$resultsSection.slideDown(250);
				} else {
					$resultsSection.slideDown(250)
					setTextWithTimeout(
						hit_counter, "Keine Treffer", 2000,
						function () { $resultsSection.slideUp(250); });
				}

				function disableElementTemporarily(element, time) {
					element.prop("disabled", true);
					setTimeout(function () {
						element.prop("disabled", false);
					}, time);
				}
			});

			// Reset search
			function resetSearch() {
				found = search_result_number = null;
				search_button.prop("disabled", false);
				$resultsSection.slideUp(250, function () {
					$("button.search-arrow").remove();
					hit_counter.empty();
				});
			}

			// Show search result in the tree
			function show_search_result(found, hit_counter) {
				collapsSiblings(found[search_result_number]);
				expandRecursive(found[search_result_number]);
				update(root);
				// Remove highlight of already selected nodes
				svgGroup.selectAll("circle.nodeCircle.selected")
					.each(function (d, i) {
						if (d.timer) {
							clearTimeout(d.timer);
							delete d.timer;
						}
						d3.select(this).classed("selected", false);
					});
				// Highlight new-found node
				var selectedNodeCircle = svgGroup.selectAll("circle.nodeCircle")
					.data(tree.nodes(root), function (d) {
						return d.id;
					})
					.filter(function (d, i) {
						return (d.id == found[search_result_number].id);
					})
					.classed("selected", true)
					.each(function (d, i) {
						var thisNode = this;
						if (d.timer) {
							clearTimeout(d.timer);
						}
						d.timer = setTimeout(function () {
							d3.select(thisNode).classed("selected", false);
							delete d.timer;
						}, 5000);
					});

				centerNode(found[search_result_number]);
				hit_counter.text("Treffer " + (search_result_number + 1) + " von " + found.length);
			}

			function search(node, search_term) {
				var labelsInLanguage = findLabelsOfAppropriateLanguage(node);
				var hit = false;
				var i = 0;
				while (!hit && i < labelsInLanguage.length) {
					if (testLabel(labelsInLanguage[i].text, search_term) || testLabelVariants(labelsInLanguage[i].variants, search_term)) {
						hit = true;
					} else {
						i++;
					}
				}

				if (hit) {
					found.push(node);
				}

				var children = getChildren(node);
				if (children) {
					for (var n = 0; n < children.length; n++) {
						search(children[n], search_term);
					}
				}
			}

			function testLabel(labelText, search_term) {
				return compare(labelText.toLowerCase().trim(), search_term);
			}

			function testLabelVariants(variants, search_term) {
				if (variants) {
					for (var i = 0; i < variants.length; i++) {
						if (compare(variants[i].text.toLowerCase().trim(), search_term)) {
							return true;
						}
					}
				}
				return false;
			}

			function compare(text, search_term) {
				var comparingMethod = searchMethodSelect.find("option:selected").data("method");
				return comparingMethod(text, search_term);
			}

			// Start search if the 'enter'-button is pressed
			search_field.on("keyup", function (event) {
				event.preventDefault();
				if (13 === event.keyCode) {
					var field = $(this);
					field.autocomplete("close");
					field.blur();
					search_button.click();
				}
			});

			// Input autocomplete
			search_field.autocomplete({
					source: function (request, response) {
						var term = request.term.toLowerCase().trim();
						if (!term) {
							response(searchInputCache);
						} else {
							var proposals = [];
							for (var i = 0; i < searchInputCache.length; i++) {
								if (searchInputCache[i].startsWith(term)) {
									proposals.push(searchInputCache[i]);
								}
							}
							response(proposals);
						}
					},
					select: function (event, ui) {
						$(this).val(ui.item.value).blur();
						search_button.click();
					}
				})
				.autocomplete("instance")._renderItem = function (ul, item) {
					var li = $("<li/>")
						.attr("data-value", item.value)
						.appendTo(ul);
					var itemSpan = $("<span>" + item.label + "</span>").appendTo(li);
					$("<span class='close-cross'>\u2715</span>")
						.on("click", function (event) {
							var index = searchInputCache.indexOf(item.value);
							searchInputCache.splice(index, 1);
							$(this).closest("li").remove();
						})
						.appendTo(itemSpan);
					return li;
				};

			function storeSearchTermInAutocompleteCache(term) {
				var index = searchInputCache.indexOf(term);
				if (0 !== index) {  // term is not already at first position
					if (0 < index) {
						searchInputCache.splice(index, 1);
					}
					searchInputCache.unshift(term);
				}
			}

			function collapsSiblings(node) {
				if (node.parent) {
					expand(node.parent);
					var siblings = node.parent.children;
					for (var n = 0; n < siblings.length; n++) {
						if (siblings[n] !== node) {
							collapse(siblings[n]);
						}
					}
					collapsSiblings(node.parent);
				}
			}

			function expandRecursive(node) {
				expand(node);
				var children = getChildren(node);
				if (children) {
					for (var n = 0; n < children.length; n++) {
						expandRecursive(children[n]);
					}
				}
			}

			function getChildren(node) {
				if (node.children) {
					return node.children;
				} else if (node._children) {
					return node._children;
				} else {
					return null;
				}
			}
		}

		/* end search and filter section */
		/* ***************************** */


		// focus on selected node
		function focusOnSelectedNode(id) {
			svgGroup.selectAll("circle.nodeCircle")
				.data(tree.nodes(root), function (d) {
					return d.id;
				})
				.filter(function (d, i) {
					return (d.id == id);
				})
				.classed("selected", true)
				.each(function (d) {
					centerNode(d);
				});
		}
	});
}




// *************************************
// tree traversing

//traverses recursively the tree nodes
function traverseTree(node, rootNode = true) {
	var nodeData = {
		id: node.id,
		labels: node.labels,
		definitions: node.definitions,
		references: node.references,
		external_references: node.external_references
	};

	if (!rootNode) {
		nodeData.sort_order_hits = node.sort_order_hits;
	}

	processChildren(node, nodeData, function (child) {
		return traverseTree(child, false);
	});

	return nodeData;
}

//traverses recursively the tree nodes and replaces all IDs
function traverseTreeAndReplaceID(node, conceptIDs = false, rootNode = true) {
	var nodeData = {
		labels: replaceIDs(node.labels, "variants"),
		definitions: replaceIDs(node.definitions),
		references: replaceIDs(node.references),
		external_references: replaceIDs(node.external_references)
	};

	if (conceptIDs) {
		if (true === conceptIDs) {
			conceptIDs = {
				"id": 0
			};
		}
		nodeData.id = --(conceptIDs.id);
	}

	if (!rootNode) {
		nodeData.sort_order_hits = replaceIDs(node.sort_order_hits);
	}

	function replaceIDs(attributes, descendant, first = true) {
		if (Array.isArray(attributes)) {
			var cleanedAttributes = [];
			for (var i = 0; i < attributes.length; i++) {
				if (first) {
					var attribute = $.extend(true, {}, attributes[i]); // deep copy
				} else {
					var attribute = attributes[i];
				}
				attribute.id = -(i + 1);
				cleanedAttributes.push(attribute);
				if (descendant) {
					attribute[descendant] = replaceIDs(attribute[descendant], false);
				}
			}
			return cleanedAttributes;
		} else {
			return null;
		}
	}

	processChildren(node, nodeData, function (child) {
		return traverseTreeAndReplaceID(child, conceptIDs, false);
	});

	return nodeData;
}

// process the children of a given node
function processChildren(node, nodeData, processChildFunction) {
	var children = null;
	if (node.children) {
		children = node.children;
	} else if (node._children) {
		children = node._children;
	}
	if (children) {
		nodeData.children = [];
		for (var i = 0; i < children.length; i++) {
			nodeData.children.push(processChildFunction(children[i]));
		}
	}
}

// end tree traversing
// *************************************


// final operations for control elements
function finalization(switches, load_status, message, failed) {
	switches.map(function (e) { e.resolve(); });
	setStatus(load_status, message, 100, failed);
}
