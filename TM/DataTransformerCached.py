from .models import Concept, Relation, Label, Definition, Variant, Reference, \
    ExternalReference, SortOrderHit


def writeDictFromRelationalModel(vocab_id, concept_id=None):
    """Method for recursively writing a dictionary from the relational
    representation of the tree structure
    """
    concepts_cache = dict()
    other_concepts_cache = dict()
    relations_cache = dict()
    cache = dict(
        labels=dict(),
        definitions=dict(),
        references=dict(),
        reverse_references=dict(),
        external_references=dict(),
        sort_order_hits=dict()
    )

    if concept_id is None:
        children = set()

    # Concepts
    concepts = Concept.objects.values()
    for concept in concepts:
        if concept["vocab_id"] == vocab_id:
            concepts_cache[concept["id"]] = concept
        else:
            other_concepts_cache[concept["id"]] = concept

    # Relations
    relations = Relation.objects \
                        .filter(type="child") \
                        .filter(this_concept__in=concepts_cache.keys()) \
                        .values()
    for relation in relations:
        __insert_element_into_cache(
            relations_cache, "other_concept_id", relation)
        if concept_id is None:
            children.add(relation["this_concept_id"])

    # Labels
    labels_per_ids = dict()
    labels = Label.objects.filter(
        concept_id__in=concepts_cache.keys()).values()
    for label in labels:
        __insert_element_into_cache(cache["labels"], "concept_id", label)
        labels_per_ids[label["id"]] = label

    # Variants
    variants = Variant.objects \
                      .filter(label_id__in=labels_per_ids.keys()) \
                      .values()
    for variant in variants:
        parent_label = labels_per_ids[variant.pop("label_id")]
        if "variants" in parent_label:
            parent_label["variants"].append(variant)
        else:
            parent_label["variants"] = [variant]

    # Definitions
    definitions = Definition.objects \
                            .filter(concept_id__in=concepts_cache.keys()) \
                            .values()
    for definition in definitions:
        __insert_element_into_cache(
            cache["definitions"], "concept_id", definition)

    # References
    references = Reference.objects \
                          .filter(concept_id__in=concepts_cache.keys()) \
                          .values()
    for reference in references:
        target_concept_id = reference["target_concept_id"]
        if reference["target_concept_id"] in concepts_cache:
            reference["target_vocab_id"] = \
                concepts_cache[target_concept_id]["vocab_id"]
        else:
            reference["target_vocab_id"] = \
                other_concepts_cache[target_concept_id]["vocab_id"]

        __insert_element_into_cache(
            cache["references"], "concept_id", reference)

    # Reverse reference
    references = Reference \
        .objects \
        .filter(target_concept_id__in=concepts_cache.keys()) \
        .values()
    for reference in references:
        reverse_reference = dict(
            id=reference["id"],
            concept_id=reference["target_concept_id"],
            target_concept_id=reference["concept_id"],
            target_vocab_id=concepts_cache[reference["concept_id"]]["vocab_id"]
            if reference["concept_id"] in concepts_cache
            else other_concepts_cache[reference["concept_id"]]["vocab_id"]
        )
        __insert_element_into_cache(
            cache["reverse_references"], "concept_id", reverse_reference)

    # External_references
    external_references = ExternalReference \
        .objects \
        .filter(concept_id__in=concepts_cache.keys()) \
        .values()
    for external_reference in external_references:
        __insert_element_into_cache(
            cache["external_references"], "concept_id", external_reference)

    # Sort order hits
    sort_order_hits = SortOrderHit \
        .objects \
        .filter(concept_id__in=concepts_cache.keys()) \
        .values()
    for sort_order_hit in sort_order_hits:
        __insert_element_into_cache(
            cache["sort_order_hits"], "concept_id", sort_order_hit)

    # Find root concept
    if concept_id is None:
        root = concepts_cache.keys() - children
        if len(root) != 1:
            return None
        root_concept = concepts_cache[root.pop()]
    else:
        if concept_id not in concepts_cache:
            return None
        root_concept = concepts_cache[concept_id]
    return __recursively_construct_tree(
        root_concept, concepts_cache, relations_cache, cache)


def __recursively_construct_tree(concept, concepts_cache, relations_cache,
                                 cache):
    """This method recursively constructs a dictionary containing the tree
    information
    """
    node = dict(id=concept["id"])

    for key in cache.keys():
        node[key] = cache[key].get(concept["id"], None)

    if concept["id"] in relations_cache:
        child_links = relations_cache[concept["id"]]
        node["children"] = list()
        for child_link in child_links:
            node["children"].append(__recursively_construct_tree(
                concepts_cache[child_link["this_concept_id"]], concepts_cache,
                relations_cache, cache))

    return node


def __insert_element_into_cache(cache, key, element):
    """Helper method for storing elements in the cache dictionary"""
    key_id = element.pop(key)
    if key_id in cache:
        cache[key_id].append(element)
    else:
        cache[key_id] = [element]
