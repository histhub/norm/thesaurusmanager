from .models import Relation, Concept, Label, Definition, Variant, Reference, \
    ExternalReference, SortOrderHit
from .DataTransformerCached import writeDictFromRelationalModel
from .DatabaseOperationManager import Inserts, Deletes
from .jsonValidator import validateJSON


def update_tree(json_root_node, vocab_id):
    """main method for tree updating"""
    # validate JSON
    validationResult = validateJSON(json_root_node)
    if not validationResult["success"]:
        raise Exception("Invalid JSON: " + str(validationResult["error"]))

    # construct cache
    db_root_node = writeDictFromRelationalModel(vocab_id)
    if db_root_node is None:
        raise Exception("Tree data is not consistent.")

    # special treatment of moved nodes
    moved_nodes = {}
    __construct_dict_for_moved_nodes(json_root_node, moved_nodes)

    # root change
    if db_root_node["id"] != json_root_node["id"]:
        # delete existing tree in DB
        deletes = Deletes({
            Concept: "id__in",
            Relation: "this_concept__in"
        })
        __delete_branch(db_root_node, moved_nodes, deletes)
        deletes.bulk_delete(Relation)
        deletes.bulk_delete(Concept)

        # insert new root node
        inserts = Inserts(Concept)
        new_concept = __create_new_concept(inserts, vocab_id)

        # re-construct cache
        db_root_node = dict(id=new_concept.id)
        # update json_root_node
        json_root_node["id"] = new_concept.id

        inserts.bulk_insert(Concept)

    __recursively_delete_nodes(db_root_node, json_root_node, moved_nodes)
    __recursively_update_and_insert_nodes(
        db_root_node, json_root_node, vocab_id, moved_nodes)


def __delete_branch(db_node, moved_nodes, deletes):
    # Deletes recursively a whole branch (with all descendants) from the tree
    if "children" in db_node:
        for child in db_node["children"]:
            __delete_branch(child, moved_nodes, deletes)

    deletes.delete(Relation, db_node["id"])
    # Nodes moved within the tree are not deleted
    # because they will be needed later
    if db_node["id"] in moved_nodes:
        if "children" in db_node:
            del db_node["children"]
        moved_nodes[db_node["id"]] = db_node
    else:
        deletes.delete(Concept, db_node["id"])


def __recursively_delete_nodes(db_node, json_node, moved_nodes, deletes=None):
    """Traverses the tree recursively finding all nodes that have to be deleted
    because they are not part of the new trees
    """
    if not deletes:
        deletes = Deletes({
            Concept: "id__in",
            Relation: "this_concept__in"
        })
        first_call = True
    else:
        first_call = False

    (db_children, json_children) = __construct_two_dicts_with_the_children(
        db_node, json_node)

    # Remove superfluous children
    for delete_child_id in db_children.keys() - json_children.keys():
        __delete_branch(db_children[delete_child_id], moved_nodes, deletes)
        db_node["children"].remove(db_children[delete_child_id])

    (db_children, json_children) = __construct_two_dicts_with_the_children(
        db_node, json_node)

    # continue recursively with the children
    for child_id in db_children.keys():
        __recursively_delete_nodes(
            db_children[child_id], json_children[child_id], moved_nodes,
            deletes)

    # bulk delete operations
    if first_call:
        deletes.bulk_delete(Relation)
        deletes.bulk_delete(Concept)


class AttributeConfiguration:
    """Class for storing the attribute configuration"""
    def __init__(self, key, attribute_list, model, fk_to_parent,
                 descendant=None):
        self.key = key
        self.attribute_list = attribute_list
        self.model = model
        self.fk_to_parent = fk_to_parent
        self.descendant = descendant


ATTRIBUTE_CONFIGURATIONS = [
    AttributeConfiguration(
        "labels", ["text", "lang", "is_preferred"], Label, "concept_id",
        AttributeConfiguration("variants", ["text", "comment"], Variant,
                               "label_id")),
    AttributeConfiguration("definitions", ["text", "lang"], Definition,
                           "concept_id"),
    AttributeConfiguration("references", ["target_concept_id"], Reference,
                           "concept_id"),
    AttributeConfiguration("external_references", ["target_url"],
                           ExternalReference, "concept_id"),
    AttributeConfiguration("sort_order_hits", ["position_hit"], SortOrderHit,
                           "concept_id"),
    ]


def __get_model_classes(attribute_configurations):
    """Helper function for getting the model classes from the attribute
    configuration list
    """
    model_classes = list()
    for attribute_configuration in attribute_configurations:
        model_classes.append(attribute_configuration.model)
        if attribute_configuration.descendant:
            model_classes.append(attribute_configuration.descendant.model)
    return model_classes


def __recursively_update_and_insert_nodes(db_node, json_node, vocab_id,
                                          moved_nodes, inserts=None,
                                          deletes=None):
    """Traverses the tree recursively updating existing and inserting new nodes
    """
    if not inserts or not deletes:
        model_classes = __get_model_classes(ATTRIBUTE_CONFIGURATIONS)
        inserts = Inserts(Concept, Relation, *model_classes)

        del_conf = {}
        for model_class in model_classes:
            del_conf[model_class] = "id__in"
        deletes = Deletes(del_conf)

        first_call = True
    else:
        first_call = False

    for attribute_configuration in ATTRIBUTE_CONFIGURATIONS:
        __compare_and_update_additional_data(
            db_node, json_node, attribute_configuration, inserts, deletes)

    (db_children, json_children) = __construct_two_dicts_with_the_children(
        db_node, json_node)

    # Insert new children
    for new_child_id in json_children.keys() - db_children.keys():
        new_child = json_children[new_child_id]
        # Moved nodes already exist
        if moved_nodes[new_child["id"]]:
            # Insert link to parent json_node
            inserts.insert(Relation(
                id=0, other_concept_id=json_node["id"],
                this_concept_id=new_child["id"], type="child"))
            # Update cache
            if "children" in db_node:
                db_node["children"].append(moved_nodes[new_child["id"]])
            else:
                db_node["children"] = [moved_nodes[new_child["id"]]]
        else:
            # New concept
            new_concept = __create_new_concept(inserts, vocab_id)
            # Insert link to parent json_node
            inserts.insert(Relation(id=0, other_concept_id=json_node["id"],
                                    this_concept=new_concept, type="child"))

            # Update cache
            child = dict(id=new_concept.id)
            if "children" in db_node:
                db_node["children"].append(child)
            else:
                db_node["children"] = [child]

            new_child["id"] = new_concept.id

    (db_children, json_children) = __construct_two_dicts_with_the_children(
        db_node, json_node)

    # Continue recursively with the children
    for child_id in json_children.keys():
        __recursively_update_and_insert_nodes(
            db_children[child_id], json_children[child_id], vocab_id,
            moved_nodes, inserts, deletes)

    if first_call:
        # Bulk deletes
        for model_class in model_classes:
            deletes.bulk_delete(model_class)
        # Bulk inserts
        inserts.bulk_insert(Concept)
        inserts.bulk_insert(Relation)
        for model_class in model_classes:
            inserts.bulk_insert(model_class)


def __construct_two_dicts_with_the_children(db_node, json_node):
    """Constructs two dicts with the children of a json_node on the basis of
    the data from the DB on the one hand and the data of the JSON on the other
    hand, which allows us to compare both in the calling methods.
    """
    if "children" in db_node:
        db_children = {c["id"]: c for c in db_node["children"]}
    else:
        db_children = dict()

    if "children" in json_node:
        json_children = {c["id"]: c for c in json_node["children"]}
    else:
        json_children = dict()

    return (db_children, json_children)


def __construct_dict_for_moved_nodes(node, moved_nodes):
    """Prepares recursively a dict for storing informations about all nodes
    which are moved within the tree
    """
    moved_nodes[node["id"]] = None
    for child in node.get("children", list()):
        __construct_dict_for_moved_nodes(child, moved_nodes)


def __compare_and_update_additional_data(db_element, json_element, ac,
                                         inserts, deletes):
    """Compares the additional data (like labels, definitions) of the json_node
    and the db_node and updates the data in the DB if necessary
    """
    # TODO: Check comprehensions in this function
    if db_element.get(ac.key, None):
        db_data = {data["id"]: data for data in db_element[ac.key]}
    else:
        db_data = dict()

    if json_element.get(ac.key, None):
        json_data = {d["id"]: d for d in json_element[ac.key]}
    else:
        json_data = dict()

    # Updating already existing additional data
    for data_id in set(db_data.keys()).intersection(set(json_data.keys())):
        to_change = __compare_all_attributes(
            db_data[data_id], json_data[data_id], ac.attribute_list)
        if to_change:
            instance = ac.model.objects.get(pk=data_id)
            for attribute in to_change:
                setattr(instance, attribute, to_change[attribute])
            instance.save()

        # Descendants
        if ac.descendant:
            __compare_and_update_additional_data(
                db_data[data_id], json_data[data_id], ac.descendant, inserts,
                deletes)

    # Delete superfluous data
    for data_id in db_data.keys() - json_data.keys():
        deletes.delete(ac.model, data_id)

    # Add missing data
    for data_id in json_data.keys() - db_data.keys():
        # We use only the defined attributes
        new_data = dict()
        for attr in json_data[data_id]:
            if attr in ac.attribute_list:
                new_data[attr] = json_data[data_id][attr]
        new_data[ac.fk_to_parent] = db_element["id"]
        new_data_instance = ac.model(**new_data)
        inserts.insert(new_data_instance)
        new_data["id"] = new_data_instance.id

        # Descendants
        if ac.descendant:
            __compare_and_update_additional_data(
                new_data, json_data[data_id], ac.descendant, inserts, deletes)


def __compare_all_attributes(db_object, json_object, attribute_list):
    """Compares the two objects with regard to the attributes of the given list
    and creates a dictionary containing the information for the updates to
    perform
    """
    to_change = dict()
    for attribute in attribute_list:
        if db_object[attribute] != json_object[attribute]:
            to_change[attribute] = json_object[attribute]
    return to_change


def __create_new_concept(inserts, vocab_id):
    """Creates a new concept"""
    new_concept = Concept(id=0, vocab_id=vocab_id, identifier="--")
    inserts.insert(new_concept)
    return new_concept
