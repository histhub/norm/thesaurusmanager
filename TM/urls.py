# flake8: noqa

from django.urls import path
from . import views

app_name = 'TM'

urlpatterns = [
    # Tree visualization
    path("<int:vocab_id>/tree_data/", views.tree_data, name="tree_data"),
    path("tree_modify/", views.tree_modify, name="tree_modify"),
    path("tree/", views.tree, name="tree"),

    # Vocabulary administration
    path("vocab_create/", views.create_new_vocab, name="vocab_create"),
    path("vocab_list/", views.vocab_list, name="vocab_list"),
    path("<int:vocab_id>/vocab_detail/", views.vocab_detail, name="vocab_detail"),
    path("<int:vocab_id>/vocab_edit/", views.vocab_edit, name="vocab_edit"),
    path("<int:vocab_id>/vocab_delete/", views.vocab_delete, name="vocab_delete"),
    path("help/", views.help, name="help"),
    ]
