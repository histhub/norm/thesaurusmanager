# flake8: noqa

# https://pypi.org/project/jsonschema/
from jsonschema import validate, ValidationError

SCHEMA_BASE_URL = "http://tm.norm.histhub.ch"
SCHEMA_JSON_URL = SCHEMA_BASE_URL + "/tree-schema.json"

# JSON schema for tree data validation
SCHEMA = {
    "type": "object",
    "$id": SCHEMA_JSON_URL,
    "definitions": {},
    "$schema": "http://json-schema.org/schema#",
    "properties": {
        "id": {
            "$id": "/properties/id",
            "type": "integer"
        },
        "labels": {
            "$id": "/properties/labels",
            "type": "array",
            "items": {
                "$id": "/properties/labels/items",
                "type": "object",
                "properties": {
                    "id": {
                        "$id": "/properties/labels/items/properties/id",
                        "type": "integer"
                    },
                    "identifier": {
                        "$id": "/properties/labels/items/properties/identifier",
                        "type": ["string", "null"]
                    },
                    "lang": {
                        "$id": "/properties/labels/items/properties/lang",
                        "type": "string"
                    },
                    "text": {
                        "$id": "/properties/labels/items/properties/text",
                        "type": "string"
                    },
                    "is_preferred": {
                        "$id": "/properties/labels/items/properties/is_preferred",
                        "type": "string"
                    },
                    "variants": {
                        "$id": "/properties/labels/items/properties/variants",
                        "type": ["array", "null"],
                        "items": {
                            "$id": "/properties/labels/items/properties/variants/items",
                            "type": "object",
                            "properties": {
                                "id": {
                                    "$id": "/properties/labels/items/properties/variants/items/properties/id",
                                    "type": "integer"
                                },
                                "text": {
                                    "$id": "/properties/labels/items/properties/variants/items/properties/text",
                                    "type": "string"
                                },
                                "comment": {
                                    "$id": "/properties/labels/items/properties/variants/items/properties/comment",
                                    "type": "string"
                                }
                            },
                            "additionalProperties": False,
                            "required": ["id", "text", "comment"]
                        }
                    }
                },
                "additionalProperties": False,
                "required": ["id", "lang", "text", "is_preferred"]
            }
        },
        "definitions": {
            "$id": "/properties/definitions",
            "type": ["array", "null"],
            "items": {
                "$id": "/properties/definitions/items",
                "type": "object",
                "properties": {
                    "id": {
                        "$id": "/properties/definitions/items/properties/id",
                        "type": "integer"
                    },
                    "identifier": {
                        "$id": "/properties/definitions/items/properties/identifier",
                        "type": ["string", "null"]
                    },
                    "text": {
                        "$id": "/properties/definitions/items/properties/text",
                        "type": "string"
                    },
                    "lang": {
                        "$id": "/properties/definitions/items/properties/lang",
                        "type": "string"
                    }
                },
                "additionalProperties": False,
                "required": ["id", "text", "lang"]
            }
        },
        "references": {
            "$id": "/properties/references",
            "type": ["array", "null"],
            "items": {
                "$id": "/properties/references/items",
                "type": "object",
                "properties": {
                    "id": {
                        "$id": "/properties/references/items/properties/id",
                        "type": "integer"
                    },
                    "target_concept_id": {
                        "$id": "/properties/references/items/properties/target_concept_id",
                        "type": "integer"
                    },
                    "target_vocab_id": {
                        "$id": "/properties/references/items/properties/target_vocab_id",
                        "type": "integer"
                    }
                },
                "additionalProperties": False,
                "required": ["id", "target_concept_id", "target_vocab_id"]
            }
        },
        "external_references": {
            "$id": "/properties/external_references",
            "type": ["array", "null"],
            "items": {
                "$id": "/properties/external_references/items",
                "type": "object",
                "properties": {
                    "id": {
                        "$id": "/properties/external_references/items/properties/id",
                        "type": "integer"
                    },
                    "target_url": {
                        "$id": "/properties/external_references/items/properties/target_url",
                        "type": "string"
                    }
                },
                "additionalProperties": False,
                "required": ["id", "target_url"]
            }
        },
        "sort_order_hits": {
            "$id": "/properties/sort_order_hits",
            "type": ["array", "null"],
            "items": {
                "$id": "/properties/sort_order_hits/items",
                "type": "object",
                "properties": {
                    "id": {
                        "$id": "/properties/sort_order_hits/items/properties/id",
                        "type": "integer"
                    },
                    "position_hit": {
                        "$id": "/properties/sort_order_hits/items/properties/position_hit",
                        "type": "integer"
                    }
                },
                "additionalProperties": False,
                "required": ["id", "position_hit"]
            }
        },
        "children": {
            "$id": "/properties/children",
            "type": "array",
            "items": {
                "$ref": (SCHEMA_JSON_URL + "#")
            }
        }
    },
    "additionalProperties": False,
    "required": ["id", "labels"]
}  # noqa: E501


def validateJSON(json):
    try:
        validate(json, SCHEMA)
        return dict(success=True)
    except ValidationError as err:
        return dict(success=False, error=err)
