from django import forms
from .models import Provider
from .models import Vocabulary


class VocabForm(forms.Form):
    name = forms.CharField(label="Name", required=True)
    version = forms.DecimalField(
        label="Versionsnummer", min_value=0.0, initial=1.0, required=True)
    domain = forms.CharField(label="Bereich/Thema", required=False)
    description = forms.CharField(
        label="Beschreibung", widget=forms.Textarea, required=False)
    provider = forms.ModelChoiceField(queryset=Provider.objects.all())
    access_level = forms.ChoiceField(
        label="Zugriffsmodus", choices=Vocabulary.ACCESS_LEVEL_CHOICES,
        required=True)


class VocabCreateForm(VocabForm):
    GERMAN = 'de'
    FRENCH = 'fr'
    ENGLISH = 'en'
    ITALIAN = 'it'
    LANGUAGE_CHOICES = (
        (GERMAN, 'DE'),
        (FRENCH, 'FR'),
        (ENGLISH, 'EN'),
        (ITALIAN, 'IT'),
    )
    root_name = forms.CharField(label="Name des Wurzelknotens", required=True)
    lang = forms.ChoiceField(
        label="Sprache", choices=LANGUAGE_CHOICES, required=True)


class VocabEditForm(VocabForm):
    INTERN = 'intern'
    EXTERN = 'extern'
    STATUS_CHOICES = (
        (INTERN, 'intern'),
        (EXTERN, 'extern'),
    )
    status = forms.ChoiceField(
        label="Status", choices=STATUS_CHOICES, required=True)
    base_url = forms.CharField(label="Base URL", required=False)
