class Inserts:
    """Class for managing the database inserts"""
    def __init__(self, *args):
        self.storage = {}
        for model in args:
            self.storage[model] = {"insert": []}
            self.__initialize_id_counter(model)

    def insert(self, obj, update_id=True):
        model = type(obj)
        if update_id:
            obj.id = self.__find_insert_id(model)
        self.storage[model]["insert"].append(obj)

    def bulk_insert(self, model):
        if len(self.storage[model]["insert"]) > 0:
            model.objects.bulk_create(self.storage[model]["insert"])

    def __find_insert_id(self, model):
        self.storage[model]["id_max"] += 1
        return self.storage[model]["id_max"]

    def __initialize_id_counter(self, model):
        ids = model.objects.order_by("id").values_list("id", flat=True)
        self.storage[model]["id_max"] = ids.last() if ids else 0


class Deletes:
    """Class for managing the database deletes"""
    def __init__(self, init_params):
        self.storage = {}
        for model in init_params.keys():
            self.storage[model] = {"delete": [], "filter": init_params[model]}

    def delete(self, model, obj_id):
        self.storage[model]["delete"].append(obj_id)

    def bulk_delete(self, model):
        if 0 < len(self.storage[model]["delete"]):
            model.objects.filter(**{
                self.storage[model]["filter"]: self.storage[model]["delete"]
            }).delete()
