from django.conf import settings
from django.shortcuts import render
from django.db.models import Q
from django.db.models import Max
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
import json

from .models import Vocabulary, Concept, Label, Definition, Reference, \
    ExternalReference
from .DataTransformerCached import writeDictFromRelationalModel
from .TreeUpdaterCachedBulk import update_tree
from .forms import VocabCreateForm, VocabEditForm, VocabForm
from .jsonValidator import SCHEMA


@transaction.atomic
def tree_data(request, vocab_id):
    """View delivering the tree structure as JSON data"""
    try:
        vocab = Vocabulary.objects.get(pk=vocab_id)
    except ObjectDoesNotExist:
        return HttpResponse(
            json.dumps(dict(tree=None, vocab_id=vocab_id)), "application/json")

    if (vocab.access_level in (
            Vocabulary.PUBLIC_VISIBLE, Vocabulary.MODIFIABLE_BY_OTHERS)
            or (request.user.is_authenticated
                and vocab.owner == request.user)):
        tree = writeDictFromRelationalModel(vocab.id)
        modification_allowed = (
            request.user.is_authenticated
            and (vocab.access_level == Vocabulary.MODIFIABLE_BY_OTHERS
                 or vocab.owner == request.user))
        return HttpResponse(json.dumps(dict(
            tree=tree,
            vocab_id=vocab_id,
            modification_allowed=modification_allowed,
            authenticated=request.user.is_authenticated
        )), "application/json")
    else:
        return HttpResponse(
            json.dumps(dict(tree=None, vocab_id=vocab_id)), "application/json")


@transaction.atomic
def tree(request):
    """Main view for tree selection and graphical visualization"""

    query_filter = Q(access_level__in=(
        Vocabulary.PUBLIC_VISIBLE, Vocabulary.MODIFIABLE_BY_OTHERS))
    if request.user.is_authenticated:
        query_filter = query_filter | Q(owner=request.user)

    vocabs = Vocabulary.objects \
        .filter(query_filter) \
        .select_related("provider") \
        .order_by("name")
    vocab_selection = [dict(
        id=vocab.id,
        name=vocab.name,
        version=vocab.version,
        provider=vocab.provider.name,
        modifiable=(
            request.user.is_authenticated
            and (vocab.access_level == Vocabulary.MODIFIABLE_BY_OTHERS
                 or vocab.owner == request.user)),
        owning_state=("own" if vocab.owner == request.user else "other"),
        tree_url=reverse("TM:tree_data", args=(vocab.id,))
    ) for vocab in vocabs]

    # Find available languages
    languages = {"de", "en", "fr", "it"} \
        | {lang.strip() for lang in
           Label.objects.values_list("lang", flat=True).distinct()} \
        | {lang.strip() for lang in
           Definition.objects.values_list("lang", flat=True).distinct()}

    return render(request, "TM/tree.html", dict(
        vocab_selection=json.dumps(vocab_selection),
        languages=json.dumps(sorted(languages)),
        json_schema=json.dumps(SCHEMA)))


@login_required
@transaction.atomic
def tree_modify(request):
    """
    View for modifying the tree
    """
    if request.method == "POST":
        if "tree" in request.POST and "vocab_id" in request.POST:
            try:
                vocab = Vocabulary.objects.get(pk=request.POST.get("vocab_id"))
            except ObjectDoesNotExist:
                return HttpResponse(
                    json.dumps(dict(success=False)), "application/json")

            if (Vocabulary.MODIFIABLE_BY_OTHERS == vocab.access_level
                    or vocab.owner == request.user):
                try:
                    update_tree(json.loads(request.POST.get("tree")), vocab.id)
                    return HttpResponse(
                        json.dumps(dict(success=True)), "application/json")
                except Exception as e:
                    resp = dict(success=False)
                    if settings.DEBUG:
                        resp["error"] = str(e)
                    return HttpResponse(json.dumps(resp), "application/json")

    return HttpResponse(json.dumps(dict(success=False)), "application/json")


@login_required
@transaction.atomic
def create_new_vocab(request):
    """View for creating new trees"""
    if request.method == "POST" and request.POST.get("name", None):
        if request.POST.get("tree", None):
            # Copy of an existing tree
            form = VocabForm(request.POST)
        else:
            # New tree
            form = VocabCreateForm(request.POST)

        if form.is_valid():
            # New vocabulary
            max_vocab_id = Vocabulary.objects.aggregate(
                Max("id"))["id__max"] or 0
            vocab = Vocabulary.objects.create(
                id=(max_vocab_id + 1),
                provider=form.cleaned_data["provider"],
                status="intern",
                domain=form.cleaned_data["domain"],
                name=form.cleaned_data["name"],
                version=form.cleaned_data["version"],
                description=form.cleaned_data["description"],
                base_url=None,
                owner=request.user,
                access_level=form.cleaned_data["access_level"]
            )
            # New concept
            max_concept_id = Concept.objects.aggregate(
                Max("id"))["id__max"] or 0
            concept = Concept.objects.create(
                id=(max_concept_id + 1), identifier="--", vocab=vocab)

            if request.POST.get("tree", None):
                # Copy of an existing tree
                json_root_node = json.loads(request.POST.get("tree"))
                json_root_node["id"] = concept.id
                update_tree(json_root_node, vocab.id)
            else:
                # New label
                max_label_id = Label.objects.aggregate(
                    Max("id"))["id__max"] or 0
                Label.objects.create(
                    id=(max_label_id + 1),
                    identifier=None,
                    concept=concept,
                    lang=form.cleaned_data["lang"],
                    text=form.cleaned_data["root_name"],
                    is_preferred="true")

            return HttpResponseRedirect(
                reverse("TM:vocab_list"))
    else:
        if request.method == "POST" and request.POST.get("tree", None):
            # Copy of an existing tree
            form = VocabForm()
        else:
            # New tree
            form = VocabCreateForm()

    context = dict(form=form)

    if request.method == "POST" and request.POST.get("tree", None):
        context["tree"] = request.POST.get("tree")

    return render(request, "TM/vocab_create_form.html", context)


@login_required
@transaction.atomic
def vocab_list(request):
    """View for showing vocabulary list"""
    vocabs = Vocabulary.objects \
                       .filter(owner=request.user) \
                       .select_related("provider") \
                       .order_by("name")
    return render(request, "TM/vocab_list.html", dict(
        vocabs=[__get_vocab_attribute_as_dict(vocab) for vocab in vocabs],
        json_schema=json.dumps(SCHEMA)
    ))


@login_required
@transaction.atomic
def vocab_detail(request, vocab_id):
    """View for the details of a vocabulary"""
    try:
        vocab = Vocabulary.objects.select_related("provider").get(pk=vocab_id)
        if vocab.owner == request.user:
            vocab_dict = __get_vocab_attribute_as_dict(vocab)
            __calculate_statistics(vocab, vocab_dict)
            return render(
                request, "TM/vocab_detail.html", dict(vocab=vocab_dict))
    except ObjectDoesNotExist:
        pass
    return HttpResponseRedirect(reverse("TM:vocab_list",))


@login_required
@transaction.atomic
def vocab_edit(request, vocab_id):
    """View for editing a vocabulary"""
    try:
        vocab = Vocabulary.objects.select_related("provider").get(pk=vocab_id)
        if vocab.owner == request.user:
            if request.method == "POST":
                form = VocabEditForm(request.POST)
                if form.is_valid():
                    vocab.name = form.cleaned_data["name"]
                    vocab.version = form.cleaned_data["version"]
                    vocab.domain = form.cleaned_data["domain"]
                    vocab.description = form.cleaned_data["description"]
                    vocab.provider = form.cleaned_data["provider"]
                    vocab.status = form.cleaned_data["status"]
                    vocab.base_url = form.cleaned_data["base_url"]
                    vocab.access_level = form.cleaned_data["access_level"]
                    vocab.save()
                    return HttpResponseRedirect(
                        reverse("TM:vocab_detail", args=(vocab.id,)))
            else:
                form = VocabEditForm(
                    initial=__get_vocab_attribute_as_dict(vocab, True))
            return render(
                request, "TM/vocab_edit.html", dict(
                    vocab_id=vocab_id, form=form))
    except ObjectDoesNotExist:
        pass
    return HttpResponseRedirect(reverse("TM:vocab_list",))


@login_required
@transaction.atomic
def vocab_delete(request, vocab_id):
    """View for deleting a vocabulary"""
    try:
        vocab = Vocabulary.objects.get(pk=vocab_id)
        if vocab.owner == request.user:
            vocab.delete()
    except ObjectDoesNotExist:
        pass
    return HttpResponseRedirect(reverse("TM:vocab_list",))


def __get_vocab_attribute_as_dict(vocab, raw_choice_values=False):
    """
    Helper function for copying the attributes of a vocabulary in a dictionary
    """
    return dict(
        id=vocab.id,
        name=vocab.name,
        domain=vocab.domain,
        status=vocab.status,
        version=vocab.version,
        description=vocab.description,
        provider=vocab.provider,
        base_url=(vocab.base_url if vocab.base_url else ""),
        access_level=(vocab.access_level
                      if raw_choice_values
                      else vocab.get_access_level_display)
    )


def __calculate_statistics(vocab, vocab_dict):
    """Helper function for calculating some basic statistics about a vocabulary
    """
    concepts = Concept.objects.filter(vocab=vocab)
    vocab_dict["number_of_concepts"] = concepts.count()
    vocab_dict["number_of_labels"] = Label \
        .objects \
        .filter(concept__in=concepts) \
        .count()
    vocab_dict["number_of_definitions"] = Definition \
        .objects \
        .filter(concept__in=concepts) \
        .count()
    vocab_dict["number_of_references"] = Reference \
        .objects \
        .filter(concept__in=concepts) \
        .count()
    vocab_dict["number_of_external_references"] = ExternalReference \
        .objects \
        .filter(concept__in=concepts) \
        .count()


def help(request):
    """View presenting instructions for the main use cases"""
    return render(request, "TM/help_site.html", dict(video_files=[
        ("CreateNewThesaurus.mov", "Neuen Thesaurus anlegen"),
        ("InsertNewNodes.mov", "Neue Knoten einfügen"),
        ("RestructureAndDelete.mov", "Teilbäume umhängen und löschen"),
        ("JSON_ExportAndImport.mov", "JSON-Datei exportieren und importieren"),
        ("JSON_ExportAndImportPart.mov",
            "Teilbaum als JSON-Datei exportieren und importieren"),
        ("SavePartTreeAsNewThesaurus.mov",
            "Teilbaum als neuen Thesaurus speichern"),
        ("StorePartTreeForAttachingToAnotherTree.mov",
            "Teilbaum zwischenspeichern, um ihn an einem anderen Ort "
            "wieder einzuhängen")
    ]))
