from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Provider(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField()
    url = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = "provider"

    def __str__(self):
        url = " (" + self.url + ")" if self.url else ""
        description = ", " + self.description if self.description else ""
        return self.name + url + description


class Vocabulary(models.Model):
    PRIVATE = "private"
    PUBLIC_VISIBLE = "public_visible"
    MODIFIABLE_BY_OTHERS = "modifiable_by_others"
    ACCESS_LEVEL_CHOICES = (
        (PRIVATE, "privat"),
        (PUBLIC_VISIBLE, "öffentlich sichtbar"),
        (MODIFIABLE_BY_OTHERS, "durch andere Nutzer veränderbar")
    )

    id = models.IntegerField(primary_key=True)
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT)
    status = models.TextField(blank=True, null=True)
    domain = models.TextField(blank=True, null=True)
    name = models.TextField()
    version = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    base_url = models.TextField(blank=True, null=True)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    access_level = models.CharField(
        max_length=20, choices=ACCESS_LEVEL_CHOICES)

    class Meta:
        managed = True
        db_table = "vocabulary"


class Concept(models.Model):
    id = models.IntegerField(primary_key=True)
    identifier = models.TextField()
    vocab = models.ForeignKey(Vocabulary, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = "concept"

    def __str__(self):
        name = self.__class__.__name__
        params = dict(id=self.id, vocab=self.vocab_id,
                      identifier=self.identifier)
        return '%s(%s)' % (
            name, ', '.join('%s=%s' % p for p in params.items()))


class Definition(models.Model):
    id = models.IntegerField(primary_key=True)
    identifier = models.TextField(blank=True, null=True)
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    lang = models.TextField()
    text = models.TextField()

    class Meta:
        managed = True
        db_table = "definition"


class ExternalReference(models.Model):
    id = models.IntegerField(primary_key=True)
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    target_url = models.TextField()

    class Meta:
        managed = True
        db_table = "external_reference"


class Label(models.Model):
    id = models.IntegerField(primary_key=True)
    identifier = models.TextField(blank=True, null=True)
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    lang = models.TextField()
    text = models.TextField()
    is_preferred = models.TextField()

    class Meta:
        managed = True
        db_table = "label"


class Reference(models.Model):
    id = models.IntegerField(primary_key=True)
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    target_concept = models.ForeignKey(
        Concept, related_name="reference_target_concept",
        on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = "reference"


class Relation(models.Model):
    id = models.IntegerField(primary_key=True)
    identifier = models.TextField(blank=True, null=True)
    this_concept = models.ForeignKey(
        Concept, on_delete=models.CASCADE, related_name="subj")
    other_concept = models.ForeignKey(
        Concept, on_delete=models.CASCADE, related_name="obj")
    type = models.TextField()

    class Meta:
        managed = True
        db_table = "relation"


class SortOrderHit(models.Model):
    id = models.IntegerField(primary_key=True)
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    position_hit = models.IntegerField()

    class Meta:
        managed = True
        db_table = "sort_order_hit"


class Variant(models.Model):
    id = models.IntegerField(primary_key=True)
    label = models.ForeignKey(Label, on_delete=models.CASCADE)
    text = models.TextField()
    comment = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = "variant"
