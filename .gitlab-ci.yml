---
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: python:3.11

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  VENV: './venv'
  PYTHON: '/usr/local/bin/python3.11'

  DS_DEFAULT_ANALYZERS: 'gemnasium-python'
  DS_DISABLE_DIND: 'true'
  DS_PYTHON_VERSION: 3

  SAST_DEFAULT_ANALYZERS: 'bandit, secrets'
  SAST_DISABLE_DIND: 'true'

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  key:
    files:
      - requirements.txt
    prefix: "${CI_COMMIT_REF_SLUG}-${CI_JOB_NAME}"
  paths:
    - .cache/pip
    - venv

stages:
  - lint
  - test
  - scan


.setup_venv: &setup_venv
  - $PYTHON -V
  - make virtualenv

lint:
  stage: lint
  before_script:
    - *setup_venv
    - make $VENV/.flake8_installed
  script:
    - make lint


include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml

.security_scanners: &security_scanners
  stage: scan
  after_script:
    - rm $CI_PROJECT_DIR/requirements.txt

dependency_scanning:
  <<: *security_scanners

gemnasium-python-dependency_scanning:
  before_script:
    - |
      # Ensure pg_config is installed (required to compile psycopg2)
      if command -v apk >/dev/null 2>&1
      then
        apk add --no-cache postgresql-dev
      elif command -v apt-get >/dev/null 2>&1
      then
        apt-get -y update
        apt-get -y install libpq-dev
      fi

sast:
  <<: *security_scanners
