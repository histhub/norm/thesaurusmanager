"""
WSGI config for ThesaurusManager project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application


def application(environ, *args, **kwargs):
    # http://blog.dscpl.com.au/2012/10/requests-running-in-wrong-django.html
    os.environ["DJANGO_SETTINGS_MODULE"] = "ThesaurusManager.settings"

    # Copy certain variables from the web server's environ over to os.environ
    def env_copy(key):
        return any((key.startswith(prefix) for prefix in ["TM_", "DJANGO_"]))
    os.environ.update(filter(lambda x: env_copy(x[0]), environ.items()))

    return get_wsgi_application()(environ, *args, **kwargs)
